//Moment

const buildAddBPRequest = require("./add_bp_request");
const buildEditBPRequest = require("./edit_bp_request");

const addBPRequest = buildAddBPRequest();
const editBPRequest = buildEditBPRequest();

module.exports = {
  addBPRequest,
  editBPRequest
};
