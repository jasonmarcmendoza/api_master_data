module.exports = function buildAddBPRequest() {
  return async function makeAddBPRequest(requestInfo) {
    const {
      U_COMPANY_CODE,
      U_SIGNATORY_CODE,
      U_REQUEST_TYPE,
      U_CREATED_BY,
      RequestDetails,
      BPAddresses,
      ContactEmployees
    } = requestInfo;

    if (!U_COMPANY_CODE) {
      throw new Error("Company code must be provided.");
    }
    if (!U_CREATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (!U_REQUEST_TYPE) {
      throw new Error("Request type must be provided.");
    }

    if (!RequestDetails || RequestDetails.length < 1) {
      throw new Error("Request Details must be provided");
    }
    for await (details of RequestDetails) {
      const {
        U_CARD_TYPE,
        U_NAME,
        U_CURRENCY,
        U_LIC_TRADE_NUM,
        U_GROUP_NUM,
        U_LIST_NUM,
        U_PHONE1,
        U_DEB_PAY_ACCT
      } = details;

      if (!U_CARD_TYPE) {
        throw new Error("Card Type must be provided.");
      }
      if (U_CARD_TYPE != "S" && U_CARD_TYPE != "C") {
        throw new Error("Invalid card type.");
      }

      if (U_CARD_TYPE == "S" && !U_SIGNATORY_CODE) {
        throw new Error("Signatory must be provided");
      }
      if (!U_NAME) {
        throw new Error("Name must be provided.");
      }
      if (!U_CURRENCY) {
        throw new Error("Currency must be provided.");
      }
      if (!U_LIC_TRADE_NUM) {
        throw new Error("Tax ID must be provided.");
      }
      if (!U_GROUP_NUM) {
        throw new Error("Payment terms must be provided.");
      }
      if (!U_LIST_NUM) {
        throw new Error("Price list must be provided.");
      }
      if (!U_PHONE1) {
        throw new Error("Telephone 1 must be provided,");
      }
      if (!U_DEB_PAY_ACCT) {
        throw new Error("Payable/Receivable account must be provided.");
      }
    }

    if (!BPAddresses || BPAddresses.length < 1) {
      throw new Error("BP Addresses must be provided.");
    }
    for await (address of BPAddresses) {
      const { U_ADDRESS_TYPE, U_ADDRESS, U_STREET, U_COUNTRY } = address;
      if (!U_ADDRESS_TYPE) {
        throw new Error("Address type must be provided.");
      }
      if (U_ADDRESS_TYPE != "B" && U_ADDRESS_TYPE != "S") {
        throw new Error("Invalid address type.");
      }
      if (!U_ADDRESS) {
        throw new Error("Address name must be provided.");
      }
      if (!U_STREET) {
        throw new Error("Street must be provided.");
      }
      if (!U_COUNTRY) {
        throw new Error("Country must be provided.");
      }
    }

    if (!ContactEmployees || ContactEmployees.length < 1) {
      throw new Error("Contact Persons must be provided.");
    }
    for await (contact of ContactEmployees) {
      const { U_NAME, U_FIRST_NAME, U_LAST_NAME } = contact;

      if (!U_NAME) {
        throw new Error("Contact ID must be provided");
      }
      if (!U_FIRST_NAME) {
        throw new Error("First name must be provided.");
      }
      if (!U_LAST_NAME) {
        throw new Error("Last name must be provided.");
      }

      contact.U_ACTIVE = "Y";
    }
  };
};
