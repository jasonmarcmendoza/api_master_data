module.exports = function buildEditSignatory() {
  return async function makeEditSignatory(Code, signatoryInfo) {
    const {
      U_EMPLOYEE_CODE,
      U_COMPANY_CODE,
      U_IS_ACTIVE,
      U_UPDATED_BY
    } = signatoryInfo;

    if (!Code) {
      throw new Error("Signatory code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Invalid signatory code.");
    }
    if (!U_EMPLOYEE_CODE) {
      throw new Error("Employee code must be provided.");
    }
    if (!U_COMPANY_CODE) {
      throw new Error("Company code must be provided.");
    }
    if (U_IS_ACTIVE != 1 && U_IS_ACTIVE !== 0) {
      throw new Error("Invalid status.");
    }
    if (!U_UPDATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
