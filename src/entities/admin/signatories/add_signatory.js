module.exports = function buildAddSignatory() {
  return async function makeAddSignatory(signatoryInfo) {
    const { U_EMPLOYEE_CODE, U_COMPANY_CODE, U_CREATED_BY } = signatoryInfo;

    if (!U_EMPLOYEE_CODE) {
      throw new Error("Employee code must be provided.");
    }
    if (!U_COMPANY_CODE) {
      throw new Error("Company code must be provided.");
    }
    if (!U_CREATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_CREATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
