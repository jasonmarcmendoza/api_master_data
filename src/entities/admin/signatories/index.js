//Moment

const buildAddSignatory = require("./add_signatory");
const buildEditSignatory = require("./edit_signatory");

const addSignatory = buildAddSignatory();
const editSignatory = buildEditSignatory();

module.exports = {
  addSignatory,
  editSignatory
};
