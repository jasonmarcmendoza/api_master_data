module.exports = function buildEditAction() {
  return async function makeEditAction(Code, actionInfo) {
    const {
      U_ACTION_NAME,
      U_IS_ACTIVE,
      U_MODULE_CODE,
      U_UPDATED_BY
    } = actionInfo;
    if (!Code) {
      throw new Error("Action code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Action code must be a number.");
    }

    if (!U_ACTION_NAME) {
      throw new Error("Action name must be provided.");
    }
    if (U_IS_ACTIVE != 1 && U_IS_ACTIVE != 0) {
      throw new Error("Invalid status.");
    }

    if (!U_MODULE_CODE) {
      throw new Error("Module code must be provided.");
    }
    if (isNaN(U_MODULE_CODE)) {
      throw new Error("Module code must be a number.");
    }
    if (!U_UPDATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
