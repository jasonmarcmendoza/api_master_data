module.exports = function buildAddAction() {
  return async function makeAddAction(actionInfo) {
    const { U_ACTION_NAME, U_MODULE_CODE, U_CREATED_BY } = actionInfo;

    if (!U_ACTION_NAME) {
      throw new Error("Action name must be provided.");
    }

    if (!U_MODULE_CODE) {
      throw new Error("Module code must be provided.");
    }
    if (isNaN(U_MODULE_CODE)) {
      throw new Error("Module code must be a number.");
    }
    if (!U_CREATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_CREATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
