//Moment

const buildAddAction = require("./add_action");
const buildEditAction = require("./edit_action");

const addAction = buildAddAction();
const editAction = buildEditAction();

module.exports = {
  addAction,
  editAction
};
