module.exports = function buildAddRole() {
  return async function makeAddRole(roleInfo) {
    const { Name, U_CREATED_BY } = roleInfo;

    if (!Name) {
      throw new Error("Role name must be provided.");
    }

    if (!U_CREATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_CREATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
