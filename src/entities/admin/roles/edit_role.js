module.exports = function buildEditRole() {
  return async function makeEditRole(Code, roleInfo) {
    const { Name, U_IS_ACTIVE, U_UPDATED_BY } = roleInfo;

    if (!Code) {
      throw new Error("Role code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Role code must be a number.");
    }
    if (!Name) {
      throw new Error("Role name must be provided.");
    }
    if (!Name) {
      throw new Error("Role name must be provided.");
    }
    if (U_IS_ACTIVE !== 1 && U_IS_ACTIVE !== 0) {
      throw new Error("Invalid status.");
    }

    if (!U_UPDATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
