//Moment

const buildAddRole = require("./add_role");
const buildEditRole = require("./edit_role");

const addRole = buildAddRole();
const editRole = buildEditRole();

module.exports = {
  addRole,
  editRole
};
