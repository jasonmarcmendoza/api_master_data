module.exports = function buildAddAccessRights() {
  return async function makeAddAccessRights(Code, actions, U_CREATED_BY) {
    if (!actions) {
      throw new Error("Actions must be provided.");
    }
    if (!Array.isArray(actions)) {
      throw new Error("Actions must be an array.");
    }
    if (!U_CREATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_CREATED_BY)) {
      throw new Error("Code must be a number.");
    }
    if (!Code) {
      throw new Error("Role code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Role code must be a number.");
    }
    for await (action of actions) {
      const { U_ACTION_CODE } = action;

      if (!U_ACTION_CODE) {
        throw new Error("Action code must be provided.");
      }
      if (isNaN(U_ACTION_CODE)) {
        throw new Error("Action code must be a number.");
      }
    }
  };
};
