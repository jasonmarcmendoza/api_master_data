module.exports = function buildEditAccessRights() {
  return async function makeEditAccessRights(actions, U_UPDATED_BY) {
    if (!actions) {
      throw new Error("Actions must be provided.");
    }
    if (!Array.isArray(actions)) {
      throw new Error("Actions must be an array.");
    }
    if (!U_UPDATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Code must be a number.");
    }

    for await (action of actions) {
      const { Code, role_id, U_ACTION_CODE, U_IS_ACTIVE } = action;

      if (!Code) {
        throw new Error("Access rights code must be provided.");
      }
      if (isNaN(Code)) {
        throw new Error("Access rights code must be a number.");
      }
      if (!U_ACTION_CODE) {
        throw new Error("Action code must be provided.");
      }
      if (isNaN(U_ACTION_CODE)) {
        throw new Error("Action cide must be a number.");
      }
      if (U_IS_ACTIVE !== 1 && U_IS_ACTIVE !== 0) {
        throw new Error(`Invalid status for access right id (${Code}).`);
      }
    }
  };
};
