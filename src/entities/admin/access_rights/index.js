//Moment

const buildAddAccessRights = require("./add_access_rights");
const buildEditAccessRights = require("./edit_access_rights");

const addAccessRights = buildAddAccessRights();
const editAccessRights = buildEditAccessRights();

module.exports = {
  addAccessRights,
  editAccessRights
};
