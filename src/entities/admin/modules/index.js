//Moment

const buildAddModule = require("./add_module");
const buildEditModule = require("./edit_module");

const addModule = buildAddModule();
const editModule = buildEditModule();

module.exports = {
  addModule,
  editModule
};
