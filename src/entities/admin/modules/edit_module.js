module.exports = function buildEditModule() {
  return async function makeEditModule(Code, moduleInfo) {
    const { Name, U_IS_ACTIVE, U_UPDATED_BY } = moduleInfo;

    if (!Code) {
      throw new Error("Module code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Invalid module code.");
    }
    if (!Name) {
      throw new Error("Module name must be provided.");
    }
    if (U_IS_ACTIVE != 1 && U_IS_ACTIVE !== 0) {
      throw new Error("Invalid status.");
    }
    if (!U_UPDATED_BY) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Code must be a number.");
    }
  };
};
