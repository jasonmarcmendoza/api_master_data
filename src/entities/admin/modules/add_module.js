module.exports = function buildAddModule() {
  return async function makeAddModule(moduleInfo) {
    const { Name, Code } = moduleInfo;

    if (!Name) {
      throw new Error("Module name must be provided.");
    }
    if (!Code) {
      throw new Error("Code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Code must be a number.");
    }
  };
};
