module.exports = function buildAddUser() {
  return async function makeAddUser(userInfo) {
    const { U_EMPLOYEE_CODE, U_ROLE_CODE, Code, U_USERNAME } = userInfo;

    if (!U_EMPLOYEE_CODE) {
      throw new Error("Employee code must be provided.");
    }
    if (!U_USERNAME) {
      throw new Error("Employee username must be provided.");
    }

    if (!U_ROLE_CODE) {
      throw new Error("Role id must be provided.");
    }
    if (isNaN(U_ROLE_CODE)) {
      throw new Error("Invalid role id.");
    }
    if (!Code) {
      throw new Error("User code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Invalid user code.");
    }
  };
};
