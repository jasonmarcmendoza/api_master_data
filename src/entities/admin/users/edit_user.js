module.exports = function buildEditUser() {
  return async function makeEditUser(Code, userInfo) {
    const {
      U_EMPLOYEE_CODE,
      U_ROLE_CODE,
      U_IS_ACTIVE,
      U_UPDATED_BY
    } = userInfo;

    if (!Code) {
      throw new Error("User id must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Invalid user id.");
    }
    if (!U_EMPLOYEE_CODE) {
      throw new Error("User name must be provided.");
    }
    if (isNaN(U_EMPLOYEE_CODE)) {
      throw new Error("Invalid user name.");
    }

    if (!U_ROLE_CODE) {
      throw new Error("Role id must be provided.");
    }
    if (isNaN(U_ROLE_CODE)) {
      throw new Error("Invalid role id.");
    }
    if (U_IS_ACTIVE != 0 && U_IS_ACTIVE != 1) {
      throw new Error("Invalid status.");
    }
    if (!U_UPDATED_BY) {
      throw new Error("Updated by must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Invalid updated by id.");
    }
  };
};
