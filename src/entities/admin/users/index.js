//Moment

const buildAddUser = require("./add_user");
const buildEditUser = require("./edit_user");

const addUser = buildAddUser();
const editUser = buildEditUser();

module.exports = {
  addUser,
  editUser
};
