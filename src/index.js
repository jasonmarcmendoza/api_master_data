const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
require("dotenv").config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const notFound = require("./controllers/not_found/not_found");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.get("/", (req, res) => {
  res.send("Master Data Management API");
});

app.use("/login", require("./routes/login"));

/********BP REQUESTS */
app.use("/api/bp_requests/", require("./routes/bp_requests"));
/********BP REQUESTS */

/********SAP */
//Suppliers
app.use("/api/suppliers", require("./routes/sap/suppliers"));
//Customners
app.use("/api/customers", require("./routes/sap/customers"));
//Companies
app.use("/api/companies", require("./routes/sap/companies"));
//Employees
app.use("/api/employees", require("./routes/sap/employees"));
//BP Groups
app.use("/api/bp_groups", require("./routes/sap/bp_groups"));
//Currencies
app.use("/api/currencies", require("./routes/sap/currencies"));
//Sales Employees
app.use("/api/sales_employees", require("./routes/sap/sales_employees"));
//Payment Terms
app.use("/api/payment_terms", require("./routes/sap/payment_terms"));
//Price List
app.use("/api/price_list", require("./routes/sap/price_list"));
//Countries
app.use("/api/countries", require("./routes/sap/countries"));
//States
app.use("/api/states", require("./routes/sap/states"));
//Chart of Accounts
app.use("/api/chart_of_accounts", require("./routes/sap/chart_of_accounts"));
/********SAP */

//*******ADMIN */
//Modules
app.use("/api/admin/modules", require("./routes/admin/modules"));
//Actions
app.use("/api/admin/actions", require("./routes/admin/actions"));
//Roles
app.use("/api/admin/roles", require("./routes/admin/roles"));
//Users
app.use("/api/admin/users", require("./routes/admin/users"));
//Activity logs
app.use("/api/admin/activity_logs", require("./routes/admin/activity_logs"));
//Signatories
app.use("/api/admin/signatories", require("./routes/admin/signatories"));

app.use("/api/pa-items", require("../src/routes/pa-items/index"));
app.use("/api/pa-transactions", require("../src/routes/pa-transactions/index"));
app.use("/api/pa-reports", require("../src/routes/pa-reports/index"));

//*******ADMIN */

app.use(async (req, res) => {
  data = await notFound();
  res.status(data.status).send(data.body);
});

app.use(function(err, req, res, next) {
  res.status(500).send({ errorMsg: "Unkown error!", ...err });
});

var port = process.env.port || 3000;
app.listen(port, console.log(`Server started at port ${port}`));
