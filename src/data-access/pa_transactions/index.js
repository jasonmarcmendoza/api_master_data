const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const url = process.env.SAP_URL;

const xsjs = process.env.XSJS_URL;

const client = require("../database/index");

const transactionsDb = {
  selectOneStageById: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_STAGES('${info.id}')`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  addTransaction: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const res = await axios({
        method: "POST",
        url: `${url}/U_PA_TRANSACTIONS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  addPriceLog: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie
      const res = await axios({
        method: "POST",
        url: `${url}/U_PA_PR_LOGS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  addRequest: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_PA_REQUESTS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data.error.message
      };
      return err;
    }
  },

  addApproval: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_PA_APPROVALS`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data.error.message
      };
      return err;
    }
  },

  selectPriceLogCount: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_PR_LOGS/$count`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  selectApprovalCount: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_APPROVALS/$count`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  selectAllLevels: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_STAGES`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },
  selectTransactionCount: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_TRANSACTIONS/$count`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      console.log(e);
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  selectRequestCount: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_REQUESTS/$count`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };

      return err;
    }
  },

  selectAllTransactions: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `select a."Code" as transaction_id, a."U_STATUS" as status, b."COMPANYNAME" as company_name,
        b."NAME" as db_name, a."U_DATE_REQUESTED" as date_requested, a."U_TIME_REQUESTED" as time_requested,
          c."U_EMPLOYEE_CODE" as emp_id, c."U_COMPANY_CODE" as requester_company
          from "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" a left join "SLDDATA"."COMPANYDBS" b on a."U_COMPANY_CODE" = b."ID"
          left join "${process.env.SL_CompanyDB}"."@USERS" c on a."U_REQUESTED_BY" = c."Code"
          where a."U_DATE_REQUESTED" between '${info.date_from}' and '${info.date_to}' 
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectAllReports: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `
          select a."Code" as request_id, a."U_ITEM_ID" as item_id, b."Code" as transaction_id ,
          d."U_STAGE" as stage, b."U_REQUESTED_BY" as requested_by, b."U_DATE_REQUESTED" as date_requested,
          b."U_TIME_REQUESTED" as time_requested, a."U_STATUS" as status, a."U_PRICE" as requested_price, c."NAME" as db_name, e."U_EMPLOYEE_CODE" as emp_id,
          f."U_OLD_PRICE" as old_price, f."U_NEW_PRICE" as new_price, c."COMPANYNAME" as company_name,
          e."U_COMPANY_CODE" as requester_company, a."U_OLD_PRICE" as old_price_upon_request
          from "${process.env.SL_CompanyDB}"."@PA_REQUESTS" a
          left join "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" b on a."U_TRANSACTION_ID" = b."Code"
          left join "SLDDATA"."COMPANYDBS" c on b."U_COMPANY_CODE" = c."ID"
          left join "${process.env.SL_CompanyDB}"."@PA_STAGES" d on a."U_STAGE_ID" = d."Code"
          left join "${process.env.SL_CompanyDB}"."@USERS" e on b."U_REQUESTED_BY" = e."Code"
          left join "${process.env.SL_CompanyDB}"."@PA_PR_LOGS" f on a."Code" = f."U_REQUEST_ID"
          where a."U_STATUS" != 'Pending' and b."U_DATE_REQUESTED" between '${info.date_from}' and '${info.date_to}'
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectApproversForReports: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `
          select a.*, d."U_EMPLOYEE_CODE", d."U_COMPANY_CODE" as approver_company
          from "${process.env.SL_CompanyDB}"."@PA_APPROVALS" a
          left join "${process.env.SL_CompanyDB}"."@PA_REQUESTS" b on a."U_REQUEST_ID" = b."Code"
          left join "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" c on b."U_TRANSACTION_ID" = c."Code"
          left join "${process.env.SL_CompanyDB}"."@USERS" d on a."U_APPROVAL_BY" = d."Code" 
          where a."U_REQUEST_ID" = ${info.id} order by a."U_LEVEL" asc
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectAllRequests: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `
          select a."Code" as request_id, a."U_ITEM_ID" as item_id, b."Code" as transaction_id ,
          d."U_STAGE" as stage, b."U_REQUESTED_BY" as requested_by, b."U_DATE_REQUESTED" as date_requested,
          b."U_TIME_REQUESTED" as time_requested, a."U_STATUS" as status, a."U_PRICE" as requested_price, c."NAME" as db_name, e."U_EMPLOYEE_CODE" as emp_id, c."COMPANYNAME" as company_name, e."U_COMPANY_CODE" as requester_company
          from "${process.env.SL_CompanyDB}"."@PA_REQUESTS" a
          left join "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" b on a."U_TRANSACTION_ID" = b."Code"
          left join "SLDDATA"."COMPANYDBS" c on b."U_COMPANY_CODE" = c."ID"
          left join "${process.env.SL_CompanyDB}"."@PA_STAGES" d on a."U_STAGE_ID" = d."Code"
          left join "${process.env.SL_CompanyDB}"."@USERS" e on b."U_REQUESTED_BY" = e."Code"
          where a."U_STATUS" = 'Pending' and b."U_DATE_REQUESTED" between '${info.date_from}' and '${info.date_to}'
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneCompany: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `SELECT * FROM "SLDDATA"."COMPANYDBS" where ID = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneTransaction: async ({ info }) => {
    try {
      //const client = await hdbClient();
      const sql = `select a."Code" as request_id, b."ItemCode" as item_code, b."ItemName" as item_name , a."U_OLD_PRICE" as current_price, c."Currency" as currency,
          a."U_PRICE" as new_price, a."U_STATUS" as status
          from "${process.env.SL_CompanyDB}"."@PA_REQUESTS" a
          left join "${info.company}"."OITM" b on a."U_ITEM_ID" = b."ItemCode"
          left join "${info.company}"."ITM1" c on b."ItemCode" = c."ItemCode"
          where a."U_TRANSACTION_ID" = ${info.id} and c."PriceList" = 1`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectRequesterByRequestId: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `select c."U_EMPLOYEE_CODE" as employee_code, c."U_COMPANY_CODE" as company_code 
          from "${process.env.SL_CompanyDB}"."@PA_REQUESTS" a
          left join "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" b on a."U_TRANSACTION_ID" = b."Code"
          left join "${process.env.SL_CompanyDB}"."@USERS" c on b."U_REQUESTED_BY" = c."Code"
          where a."Code" = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectFinalApproverByRequestId: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `select b."U_EMPLOYEE_CODE" as employee_code, b."U_COMPANY_CODE" as company_code 
          from "${process.env.SL_CompanyDB}"."@PA_APPROVALS" a
         left join "${process.env.SL_CompanyDB}"."@USERS" b on a."U_APPROVAL_BY" = b."Code"
         where a."U_REQUEST_ID" = ${info.id} order by a."U_LEVEL" desc limit 1`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneRequestByIdSimple: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `select * from "${process.env.SL_CompanyDB}"."@PA_REQUESTS" where "Code" = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneTransactionSimple: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `select * from "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" where "Code" = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneTransactionWithRequests: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `select b."U_STATUS" as request_status from "${process.env.SL_CompanyDB}"."@PA_TRANSACTIONS" a 
          left join "${process.env.SL_CompanyDB}"."@PA_REQUESTS" b on a."Code" = b."U_TRANSACTION_ID"
          where a."Code" = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneRequestById: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `
          select a.*, b."Price" as current_price from "${process.env.SL_CompanyDB}"."@PA_REQUESTS" a 
          left join "${info.company}"."ITM1" b on a."U_ITEM_ID" = b."ItemCode"
          where a."Code" = ${info.id} and b."PriceList" = 1
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  sapUpdateItemPrice: async ({ info }) => {
    try {
      //const client = await hdbClient();

      const sql = `update "${info.company}"."ITM1" set "Price" = ${info.newPrice} where "ItemCode" = '${info.id}' and "PriceList" = 1`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  updateTransaction: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_PA_TRANSACTIONS('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  updateApprovals: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_PA_APPROVALS('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  updateRequests: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_PA_REQUESTS('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };

      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data.error.message
      };
      return err;
    }
  },

  selectApprovalsByRequest: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_PA_APPROVALS?$filter=U_REQUEST_ID eq '${info.id}'&$orderby=U_LEVEL asc`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  }
};

module.exports = { transactionsDb };
