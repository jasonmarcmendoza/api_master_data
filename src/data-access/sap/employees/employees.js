module.exports = function employeesDB({
  axiosRequest,
  recursiveRequest,
  moment,
  client
}) {
  return Object.freeze({
    getEmployeeInfo,
    getEmployeeUsername,
    getEmployeeInfoByID
  });

  async function getEmployeeInfo(SessionId, CompanyDB) {
    const SL_Credentials = {
      Password: process.env.SL_Password,
      UserName: process.env.SL_UserName
    };

    const login = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/Login`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...SL_Credentials,
        CompanyDB
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
    SessionId = login.data.SessionId;

    const link = `/b1s/v1/$crossjoin(EmployeesInfo, EmployeePosition)?$expand=EmployeesInfo($select=EmployeeID,FirstName,LastName,MiddleName,eMail,ApplicationUserID),EmployeePosition($select=Name)&$filter=EmployeesInfo/Position eq EmployeePosition/PositionID`;
    const result = await recursiveRequest(SessionId, link);

    // const result = await new Promise(resolve => {
    //   client.connect(function (err) {
    //     client.exec(sql, function (err, rows) {
    //       resolve(rows)
    //     });
    //   });
    // client.disconnect();
    // })
    return result;
  }

  async function getEmployeeUsername(InternalKey, CompanyDB) {
    const sql = `SELECT A."USER_CODE" "UserCode" FROM "${CompanyDB}"."OUSR" A WHERE A."INTERNAL_K" = '${InternalKey}'`;
    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });

    // const result = await axiosRequest({
    //   method: "GET",
    //   link: `/b1s/v1/Users(${InternalKey})?$select=UserCode`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   }
    // }).catch(err => {
    //   if (err.code == "ECONNREFUSED") {
    //     throw new Error("Network Error!");
    //   } else if (!err.response) throw new Error(err.message);
    //   else if (err.response.status === 502) {
    //     throw new Error(err.response.statusText);
    //   } else if (err.response.data.error) {
    //     throw new Error(err.response.data.error.message.value);
    //   } else {
    //     throw new Error(err.response.data);
    //   }
    // });

    return result[0];
  }

  async function getEmployeeInfoByID(EmployeeID, CompanyDB) {
    const sql = `SELECT 
    A."firstName" "FirstName",
    A."middleName" "MiddleName",
    A."lastName" "LastName",
    A."email" "eMail",
    A."userId" "ApplicationUserID" 
    FROM "${CompanyDB}"."OHEM" A 
    WHERE A."empID" = '${EmployeeID}'`;
    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });
    // const result = await axiosRequest({
    //   method: "GET",
    //   link: `/b1s/v1/EmployeesInfo(${EmployeeID})?$select=FirstName,LastName,MiddleName,eMail,ApplicationUserID`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   }
    // }).catch(err => {
    //   if (err.code == "ECONNREFUSED") {
    //     throw new Error("Network Error!");
    //   } else if (!err.response) throw new Error(err.message);
    //   else if (err.response.status === 502) {
    //     throw new Error(err.response.statusText);
    //   } else if (err.response.data.error) {
    //     throw new Error(err.response.data.error.message.value);
    //   } else {
    //     throw new Error(err.response.data);
    //   }
    // });

    return result[0];
  }
};
