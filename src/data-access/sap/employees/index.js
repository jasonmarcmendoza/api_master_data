//Moment
const moment = require("moment");

//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const employees = require("./employees");

const employeesDB = employees({ ...axios, moment, client });

module.exports = employeesDB;
