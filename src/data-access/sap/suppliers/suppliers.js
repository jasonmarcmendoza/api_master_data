module.exports = function suppliersDB({ axiosRequest, recursiveRequest }) {
  return Object.freeze({
    listSuppliers
  });
  async function listSuppliers(CompanyDB) {
    // var hdb    = require('hdb');
    // var client = hdb.createClient({
    //   host     : '18.136.35.41',
    //   port     : 30015,
    //   user     : 'SYSTEM',
    //   password : 'P@ssw0rd805~'
    // });

    // client.connect(function (err) {
    //   if (err) {
    //     return console.error('Connect error', err);
    //   }
    //   client.exec('select * from "DEVBFI_CRA2"."@TRANSACTION_HEADER"', function (err, rows) {
    //     if (err) {
    //       return console.error('Execute error:', err);
    //     }
    //     console.log('Results:', rows);
    //   });
    // });

    let SessionId = null;
    const SL_Credentials = {
      Password: process.env.SL_Password,
      UserName: process.env.SL_UserName
    };
    const login = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/Login`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        CompanyDB,
        ...SL_Credentials
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    SessionId = login.data.SessionId;

    const link = `/b1s/v1/BusinessPartners?$select=CardCode,CardName&$filter=CardType eq 'S'`;
    const result = await recursiveRequest(SessionId, link);

    return result;
  }
};
