//Database connection
const pool = require("../../database/index");

const axios = require("../../../axios");

const suppliers = require("./suppliers");

const suppliersDB = suppliers({ ...axios });

module.exports = suppliersDB;
