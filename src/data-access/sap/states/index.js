//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const states = require("./states");

const statesDB = states({ ...axios, client });

module.exports = statesDB;
