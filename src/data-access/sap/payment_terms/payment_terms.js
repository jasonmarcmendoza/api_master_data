module.exports = function companiesDB({
  axiosRequest,
  recursiveRequest,
  client
}) {
  return Object.freeze({
    listPaymentTerms
  });
  async function listPaymentTerms(SessionId, CompanyDB) {
    // const SL_Credentials = {
    //   UserName: process.env.SL_UserName,
    //   Password: process.env.SL_Password
    // };

    // await axiosRequest({
    //   method: "POST",
    //   link: `/b1s/v1/Login`,
    //   headers: {
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   data: {
    //     CompanyDB,
    //     ...SL_Credentials
    //   }
    // }).then(res => (SessionId = res.data.SessionId));

    // let link = `/b1s/v1/PaymentTermsTypes?$select=GroupNumber,PaymentTermsGroupName`;
    // const result = await recursiveRequest(SessionId, link);

    // return result;
    let sql = `SELECT a."GroupNum" "GroupNumber",a."PymntGroup" "PaymentTermsGroupName" FROM "${CompanyDB}"."OCTG" a`;
    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });

    return result;
  }
};
