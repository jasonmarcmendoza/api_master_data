//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const payment_terms = require("./payment_terms");

const paymentTermsDB = payment_terms({ ...axios, client });

module.exports = paymentTermsDB;
