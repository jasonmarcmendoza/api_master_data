//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const currencies = require("./currencies");

const currenciesDB = currencies({ ...axios, client });

module.exports = currenciesDB;
