//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const chart_of_accounts = require("./chart_of_accounts");

const chartOfAccountsDB = chart_of_accounts({ ...axios, client });

module.exports = chartOfAccountsDB;
