//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const sales_employees = require("./sales_employees");

const salesEmployeesDB = sales_employees({ ...axios, client });

module.exports = salesEmployeesDB;
