//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const countries = require("./countries");

const countriesDB = countries({ ...axios, client });

module.exports = countriesDB;
