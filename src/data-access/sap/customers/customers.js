module.exports = function customersDB({ axiosRequest, recursiveRequest }) {
  return Object.freeze({
    listCustomers
  });
  async function listCustomers(CompanyDB) {
    let SessionId = null;
    const SL_Credentials = {
      Password: process.env.SL_Password,
      UserName: process.env.SL_UserName
    };
    const login = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/Login`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        CompanyDB,
        ...SL_Credentials
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    SessionId = login.data.SessionId;

    const link = `/b1s/v1/BusinessPartners?$select=CardCode,CardName&$filter=CardType eq 'C'`;
    const result = await recursiveRequest(SessionId, link);

    return result;
  }
};
