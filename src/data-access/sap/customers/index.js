//Database connection
const pool = require("../../database/index");

const axios = require("../../../axios");

const customers = require("./customers");

const customersDB = customers({ ...axios });

module.exports = customersDB;
