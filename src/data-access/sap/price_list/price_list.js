module.exports = function companiesDB({
  axiosRequest,
  recursiveRequest,
  client
}) {
  return Object.freeze({
    fetchPriceList
  });
  async function fetchPriceList(SessionId, CompanyDB) {
    // const SL_Credentials = {
    //   UserName: process.env.SL_UserName,
    //   Password: process.env.SL_Password
    // };

    // await axiosRequest({
    //   method: "POST",
    //   link: `/b1s/v1/Login`,
    //   headers: {
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   data: {
    //     CompanyDB,
    //     ...SL_Credentials
    //   }
    // }).then(res => (SessionId = res.data.SessionId));

    // let link = `/b1s/v1/PriceLists?$select=PriceListNo,PriceListName`;
    // const result = await recursiveRequest(SessionId, link);

    // return result;

    let sql = `SELECT a."ListNum" "PriceListNo",a."ListName" "PriceListName" FROM "${CompanyDB}"."OPLN" a`;
    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });

    return result;
  }
};
