//Database connection
const axios = require("../../../axios");
const client = require("../../database/index");

const price_list = require("./price_list");

const priceListDB = price_list({ ...axios, client });

module.exports = priceListDB;
