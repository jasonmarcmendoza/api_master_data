//Database connection
const client = require("../../database/index");
const axios = require("../../../axios");

const bp_series = require("./bp_series");

const BPSeriesDB = bp_series({ ...axios, client });

module.exports = BPSeriesDB;
