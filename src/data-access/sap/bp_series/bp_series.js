module.exports = function BPSeriesDB({
  axiosRequest,
  recursiveRequest,
  client
}) {
  return Object.freeze({
    listBPSeries
  });
  async function listBPSeries(SessionId, CompanyDB, CardType) {
    let sql = `SELECT a."Series",a."SeriesName" FROM "${CompanyDB}"."NNM1" a WHERE "ObjectCode" = '2' AND "DocSubType" = '${CardType}'`;
    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });

    return result;
  }
};
