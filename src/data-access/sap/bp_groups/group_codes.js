module.exports = function groupCodeDB({
  axiosRequest,
  recursiveRequest,
  client
}) {
  return Object.freeze({
    listBPGroups
  });
  async function listBPGroups(SessionId, CompanyDB) {
    // const SL_Credentials = {
    //   UserName: process.env.SL_UserName,
    //   Password: process.env.SL_Password
    // };

    // await axiosRequest({
    //   method: "POST",
    //   link: `/b1s/v1/Login`,
    //   headers: {
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   data: {
    //     CompanyDB,
    //     ...SL_Credentials
    //   }
    // }).then(res => (SessionId = res.data.SessionId));

    // let link = `/b1s/v1/BusinessPartnerGroups`;
    // const result = await recursiveRequest(SessionId, link);

    // return result;

    let sql = `SELECT a."GroupCode" "Code",a."GroupName" "Name",a."GroupType" "Type" FROM "${CompanyDB}"."OCRG" a`;
    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });

    return result;
  }
};
