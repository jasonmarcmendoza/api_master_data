//Database connection
const client = require("../../database/index");
const axios = require("../../../axios");

const group_codes = require("./group_codes");

const groupCodeDB = group_codes({ ...axios, client });

module.exports = groupCodeDB;
