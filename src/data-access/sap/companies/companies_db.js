module.exports = function companiesDB({ pool }) {
  return Object.freeze({
    findCompanyCodeById,
    listCompanies
  });
  async function findCompanyCodeById(id) {
    return pool.query(`SELECT
    "public".companies.company_code,
    "public".companies.company_code
    FROM
    "public".companies
    WHERE "public".companies.company_code = ${id}`);
  }

  async function listCompanies() {
    return pool.query(`SELECT
    "public".companies.company_code,
    "public".companies.company_location,
    "public".companies.company_name
    FROM
    "public".companies
    WHERE
    "public".companies.is_active = TRUE
    `);
  }
};
