module.exports = function companiesDB({
    axiosRequest,
    recursiveRequest
}) {
    return Object.freeze({
        listCompanies
    });
    async function listCompanies(SessionId) {
        let link = `/b1s/v1/sml.svc/LIST_COMPANIES`;
        const result = await recursiveRequest(SessionId, link);

        return result;
    }
};
