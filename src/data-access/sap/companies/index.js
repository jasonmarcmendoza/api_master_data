//Database connection
const axios = require("../../../axios");

const companies = require("./companies");

const companiesDB = companies({ ...axios });

module.exports = companiesDB;
