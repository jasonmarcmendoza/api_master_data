module.exports = function modulesDB({ pool, moment }) {
  return Object.freeze({
    listRoles,
    findRoleById,
    findRoleByName,
    insertRole,
    editRole,
    // testing
    getLatestActionId
  });

  async function listRoles() {
    return pool.query(`SELECT
        "public".roles.role_id,
        "public".roles.role_name,
        "public".roles.is_active
        FROM
        "public".roles`);
  }

  async function findRoleByName(role_name) {
    return pool.query(`SELECT
      "public".roles.role_id,
      "public".roles.role_name,
      "public".roles.is_active
      FROM
      "public".roles
      WHERE "public".roles.role_name = '${role_name}'`);
  }

  async function findRoleById(role_id) {
    return pool.query(`SELECT
    "public".roles.role_id,
    "public".roles.role_name,
    "public".roles.is_active
    FROM
    "public".roles
    WHERE "public".roles.role_id = ${role_id}`);
  }
  async function insertRole(role_name, user_id) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return pool.query(
      `INSERT INTO roles (role_name,is_active,created_at,created_by,updated_at,updated_by)
    VALUES ($1,$2,$3,$4,$5,$6) 
    RETURNING "public".roles.role_id,
    "public".roles.role_name,
    "public".roles.is_active`,
      [role_name, "TRUE", date_today, user_id, date_today, user_id]
    );
  }
  async function editRole(role_id, role_name, is_active, user_id) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE roles SET role_name=($1),is_active=($2),updated_at=($3),updated_by=($4)
        WHERE role_id=($5) RETURNING   
        "public".roles.role_id,
        "public".roles.role_name,
        "public".roles.is_active`,
      [role_name, is_active, date_today, user_id, role_id]
    );
  }

  //testing
  async function getLatestActionId() {
    return pool.query(
      `SELECT role_id FROM roles ORDER BY role_id DESC LIMIT 1`
    );
  }
};
