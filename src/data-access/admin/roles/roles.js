module.exports = function modulesDB({
  axiosRequest,
  recursiveRequest,
  moment
}) {
  return Object.freeze({
    listRoles,
    findRoleById,
    findRoleByName,
    insertRole,
    editRole,
    getRoleMaxID
    // // testing
    // getLatestActionId
  });

  async function listRoles(SessionId, link) {
    const roles = await recursiveRequest(SessionId, link);

    return roles;
  }

  async function findRoleByName(Name, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ROLES?$select=Code,Name,U_IS_ACTIVE&$filter=Name eq '${Name}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
    return result.data.value;
  }

  async function findRoleById(Code, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ROLES('${Code}')?$select=Code,Name,U_IS_ACTIVE`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }

  async function getRoleMaxID(SessionId) {
    const maxValue = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ROLES/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return maxValue.data;
  }
  async function insertRole(SessionId, batch) {
    const result = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/$batch`,
      headers: {
        "Content-Type": "multipart/mixed;boundary=a",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: batch
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }
  async function editRole(SessionId, batch) {
    const result = await axiosRequest({
      method: "POST",
      link: "/b1s/v1/$batch",
      headers: {
        "Content-Type": "multipart/mixed;boundary=a",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: batch
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
    // console.log(result);
    return result.data;
  }

  // //testing
  // async function getLatestActionId() {
  //   return pool.query(
  //     `SELECT role_id FROM roles ORDER BY role_id DESC LIMIT 1`
  //   );
  // }
};
