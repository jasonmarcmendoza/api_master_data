//Moment
const moment = require("moment");

//Database connection
const axios = require("../../../axios");

const roles = require("./roles");

const rolesDB = roles({ ...axios, moment });

module.exports = rolesDB;
