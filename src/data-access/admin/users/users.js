module.exports = function usersDB({
  axiosRequest,
  recursiveRequest,
  moment,
  client
}) {
  return Object.freeze({
    listUsers,
    insertUser,
    findUserByUserName,
    findUserByName,
    findUserById,
    editUser,
    resetPassword,
    editUserPassword,
    findUserByEmployeeCode,
    findEmployeeUser
  });

  async function findEmployeeUser(EmployeeID, Company) {
    const result = await new Promise((resolve, reject) => {
      client.connect(err => {
        if (err) reject(err);

        client.exec(
          `SELECT
         B."USER_CODE" 
         FROM "${Company}"."OHEM" A 
         LEFT JOIN "${Company}"."OUSR" B ON A."userId" = B."INTERNAL_K" 
         WHERE A."empID" = '${EmployeeID}'`,
          (err, rows) => {
            if (err) {
              reject(err);
            }
            resolve(rows);
          }
        );
      });
    });

    return result;
  }

  // async function bulkInsertUsers(users) {
  //   let sql = "";

  //   for await (user of users) {
  //     sql =
  //       sql +
  //       `(${user.username},'${user.password}','${user.first_name}',${
  //         user.middle_name ? `'${user.middle_name}'` : "NULL"
  //       },
  //       '${user.last_name}',${
  //         user.suffix_name ? `'${user.suffix_name}'` : "NULL"
  //       },${user.email ? `'${user.email}'` : "NULL"},${user.role_id},${
  //         user.accounting_group_id ? user.accounting_group_id : "NULL"
  //       },'TRUE'),`;
  //   }

  //   sql = sql.substr(0, sql.length - 1);

  //   return pool.query(`INSERT INTO users (username,password,first_name,middle_name,last_name,suffix_name,email,role_id,accounting_group_id,is_active)
  //   VALUES ${sql}  RETURNING
  //   "public".users.user_id,
  //   "public".users.first_name,
  //   "public".users.middle_name,
  //   "public".users.last_name,
  //   "public".users.suffix_name,
  //   "public".users.username,
  //   "public".users.email,
  //   "public".users.role_id,
  //   "public".users.accounting_group_id,
  //   "public".users.is_active`);
  // }
  async function listUsers(SessionId, companies) {
    // const link = `/b1s/v1/sml.svc/LIST_USERS`

    const link = `/b1s/v1/U_USERS?$select=Code,U_EMPLOYEE_CODE,U_USERNAME,U_ROLE_CODE,U_IS_ACTIVE,U_COMPANY_CODE,U_COMPANY_ACCESS`;

    const users = await recursiveRequest(SessionId, link);

    const employees = {};
    const result = [];
    for await (user of users) {
      if (!employees[`${user.U_COMPANY_CODE}`]) {
        let sql = `SELECT A."empID",A."firstName" "FirstName",A."lastName" "LastName",A."middleName" "MiddleName",A."userId" "ApplicationUserID"
        FROM "${
          companies.find(company => company.ID == user.U_COMPANY_CODE).NAME
        }"."OHEM" A`;

        const result = await new Promise(resolve => {
          client.connect(function(err) {
            client.exec(sql, function(err, rows) {
              resolve(rows);
            });
          });
          client.disconnect();
        });
        employees[`${user.U_COMPANY_CODE}`] = result;
      }
      const userDetails = employees[`${user.U_COMPANY_CODE}`].find(
        employee => employee.empID == user.U_EMPLOYEE_CODE
      );
      result.push({
        ...user,
        FirstName: userDetails.FirstName,
        LastName: userDetails.LastName,
        MiddleName: userDetails.MiddleName,
        ApplicationUserID: userDetails.ApplicationUserID
      });
    }

    return result;
  }

  async function findUserByUserName(SessionId, username) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_USERS?$select=Code,U_EMPLOYEE_CODE,U_ROLE_CODE,U_IS_ACTIVE&$filter=U_USERNAME eq '${username}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result.data.value;
  }

  async function findUserByEmployeeCode(SessionId, employee_code) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_USERS?$select=Code,U_EMPLOYEE_CODE,U_ROLE_CODE,U_COMPANY_CODE,U_IS_ACTIVE&$filter=U_EMPLOYEE_CODE eq '${employee_code}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result.data.value;
  }

  async function findUserByName(
    SessionId,
    first_name,
    middle_name,
    last_name,
    suffix_name
  ) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_USERS?$select=Code,U_EMPLOYEE_CODE,U_ROLE_CODE,U_IS_ACTIVE&$filter=U_FIRST_NAME eq '${first_name}' and U_MIDDLE_NAME eq '${middle_name}' and U_LAST_NAME eq '${last_name}' and U_SUFFIX_NAME eq '${suffix_name}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (!err.response) throw new Error(err.message);
      else if (err.response && err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result.data.value;
  }

  async function findUserById(Code, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_USERS('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (!err.response) throw new Error(err.message);
      else if (err.response && err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result.data;
  }

  // async function findUserAccessByRole(role_id) {
  //   return await pool.query(`SELECT
  //   "public".actions.action_id,
  //   "public".actions.action_name,
  //   "public".modules.module_id,
  //   "public".modules.module_name,
  //   "public".access_rights.access_rights_id
  //   FROM
  //   "public".actions
  //   INNER JOIN "public".modules ON "public".actions.module_id = "public".modules.module_id
  //   INNER JOIN "public".access_rights ON "public".access_rights.action_id = "public".actions.action_id
  //   WHERE
  //   "public".access_rights.role_id = ${role_id} AND
  //   "public".actions.is_active AND
  //   "public".modules.is_active AND
  //   "public".access_rights.is_active

  //   `);
  // }

  // async function findUserByFullName(
  //   first_name,
  //   middle_name,
  //   last_name,
  //   suffix_name
  // ) {
  //   let sql = `SELECT
  //   "public".users.user_id,
  //   "public".users.first_name,
  //   "public".users.middle_name,
  //   "public".users.last_name,
  //   "public".users.suffix_name,
  //   "public".users."password",
  //   "public".users.email,
  //   "public".users.username
  //   FROM
  //   "public".users
  //   WHERE
  //   "public".users.is_active = 'TRUE' AND
  //   "public".users.first_name = '${first_name}'
  //   AND  "public".users.last_name = '${last_name}'`;
  //   if (middle_name) {
  //     sql = sql + ` AND "public".users.middle_name = '${middle_name}'`;
  //   }
  //   if (suffix_name) {
  //     sql = sql + ` AND "public".users.suffix_name ='${suffix_name}'`;
  //   }

  //   return pool.query(sql);
  // }

  async function insertUser(SessionId, userData) {
    const maxValue = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_USERS/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (!err.response) throw new Error(err.message);
      else if (err.response && err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    const result = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_USERS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...userData,
        U_PASSWORD: userData.U_USERNAME,
        Code: maxValue.data + 1,
        Name: maxValue.data + 1,
        U_IS_ACTIVE: 1,
        U_CREATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_CREATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_UPDATED_BY: userData.U_CREATED_BY,
        U_UPDATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }
  async function editUser(SessionId, userData, Code) {
    const result = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_USERS('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...userData,
        U_UPDATED_AT: ` ${moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD")}`,
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result;
  }

  async function editUserPassword(Code, U_PASSWORD, SessionId) {
    const result = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_USERS('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_PASSWORD,
        U_UPDATED_BY: Code,
        U_UPDATED_AT: ` ${moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD")}`,
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result;
  }

  async function resetPassword(Code, password, updated_by, SessionId) {
    const result = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_USERS('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_PASSWORD: password,
        U_UPDATED_BY: updated_by,
        U_UPDATED_AT: ` ${moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD")}`,
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result;
  }

  // //testing
  // async function getLatestUsername() {
  //   return pool.query(
  //     `SELECT username FROM users ORDER BY user_id DESC LIMIT 1`
  //   );
  // }

  // async function getLatestID() {
  //   return pool.query(
  //     `SELECT user_id FROM users ORDER BY user_id DESC LIMIT 1`
  //   );
  // }
};
