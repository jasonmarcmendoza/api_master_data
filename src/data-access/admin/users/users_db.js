module.exports = function usersDB({ pool, moment }) {
  return Object.freeze({
    findUserByUsername,
    findUserAccessByRole,
    findUserByFullName,
    insertUser,
    listUsers,
    findUserById,
    editUser,
    resetPassword,
    bulkInsertUsers,
    editUserPassword,
    //testing
    getLatestUsername,
    getLatestID
  });

  async function bulkInsertUsers(users) {
    let sql = "";

    for await (user of users) {
      sql =
        sql +
        `(${user.username},'${user.password}','${user.first_name}',${
          user.middle_name ? `'${user.middle_name}'` : "NULL"
        },
        '${user.last_name}',${
          user.suffix_name ? `'${user.suffix_name}'` : "NULL"
        },${user.email ? `'${user.email}'` : "NULL"},${user.role_id},${
          user.accounting_group_id ? user.accounting_group_id : "NULL"
        },'TRUE'),`;
    }

    sql = sql.substr(0, sql.length - 1);

    return pool.query(`INSERT INTO users (username,password,first_name,middle_name,last_name,suffix_name,email,role_id,accounting_group_id,is_active)
    VALUES ${sql}  RETURNING 
    "public".users.user_id,
    "public".users.first_name,
    "public".users.middle_name,
    "public".users.last_name,
    "public".users.suffix_name,
    "public".users.username,
    "public".users.email,
    "public".users.role_id,
    "public".users.accounting_group_id,
    "public".users.is_active`);
  }
  async function listUsers() {
    return pool.query(`SELECT
  "public".users.user_id,
  "public".users.first_name,
  "public".users.middle_name,
  "public".users.last_name,
  "public".users.suffix_name,
  "public".users.username,
  "public".users.email,
  "public".users.role_id,
  "public".users.accounting_group_id,
  "public".users.is_active
  FROM
  "public".users`);
  }

  async function findUserByUsername(username) {
    return await pool.query(
      `SELECT
      "public".users.user_id,
      "public".users.first_name,
      "public".users.middle_name,
      "public".users.last_name,
      "public".users.suffix_name,
      "public".users.username,
      "public".users."password",
      "public".users.email,
      "public".users.role_id,
      "public".users.accounting_group_id,
      "public".users.is_active,
      "public".roles.role_name
      FROM
      "public".users
      INNER JOIN "public".roles ON "public".users.role_id = "public".roles.role_id    
       WHERE username = '${username}'`
    );
  }

  async function findUserById(user_id) {
    return await pool.query(
      `SELECT
      "public".users.user_id,
      "public".users.first_name,
      "public".users.middle_name,
      "public".users.last_name,
      "public".users.suffix_name,
      "public".users.username,
      "public".users.email  
      FROM
      "public".users
       WHERE user_id = ${user_id}`
    );
  }

  async function findUserAccessByRole(role_id) {
    return await pool.query(`SELECT
    "public".actions.action_id,
    "public".actions.action_name,
    "public".modules.module_id,
    "public".modules.module_name,
    "public".access_rights.access_rights_id
    FROM
    "public".actions
    INNER JOIN "public".modules ON "public".actions.module_id = "public".modules.module_id
    INNER JOIN "public".access_rights ON "public".access_rights.action_id = "public".actions.action_id
    WHERE
    "public".access_rights.role_id = ${role_id} AND
    "public".actions.is_active AND
    "public".modules.is_active AND
    "public".access_rights.is_active
    
    `);
  }

  async function findUserByFullName(
    first_name,
    middle_name,
    last_name,
    suffix_name
  ) {
    let sql = `SELECT
    "public".users.user_id,
    "public".users.first_name,
    "public".users.middle_name,
    "public".users.last_name,
    "public".users.suffix_name,
    "public".users."password",
    "public".users.email,
    "public".users.username
    FROM
    "public".users
    WHERE 
    "public".users.is_active = 'TRUE' AND
    "public".users.first_name = '${first_name}'
    AND  "public".users.last_name = '${last_name}'`;
    if (middle_name) {
      sql = sql + ` AND "public".users.middle_name = '${middle_name}'`;
    }
    if (suffix_name) {
      sql = sql + ` AND "public".users.suffix_name ='${suffix_name}'`;
    }

    return pool.query(sql);
  }

  async function insertUser(
    first_name,
    middle_name,
    last_name,
    suffix_name,
    username,
    password,
    email,
    role_id,
    accounting_group_id,
    user_id
  ) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return pool.query(
      `INSERT INTO users (first_name,middle_name,last_name,suffix_name,username,password
      ,email,role_id,accounting_group_id,is_active,created_at,created_by,updated_at,updated_by)
      VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING 
      "public".users.user_id,
      "public".users.first_name,
      "public".users.middle_name,
      "public".users.last_name,
      "public".users.suffix_name,
      "public".users.username,
      "public".users.email,
      "public".users.role_id,
      "public".users.accounting_group_id,
      "public".users.is_active`,
      [
        first_name,
        middle_name,
        last_name,
        suffix_name,
        username,
        password,
        email,
        role_id,
        accounting_group_id,
        "TRUE",
        date_today,
        user_id,
        date_today,
        user_id
      ]
    );
  }
  async function editUser(
    user_id,
    first_name,
    middle_name,
    last_name,
    suffix_name,
    username,
    email,
    role_id,
    accounting_group_id,
    is_active,
    updated_by
  ) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return pool.query(
      `UPDATE users SET first_name=($1),middle_name=($2),last_name=($3),suffix_name=($4),
    username=($5),email=($6),role_id=($7),accounting_group_id=($8),is_active=($9),updated_at=($10),updated_by=($11)
    WHERE user_id=($12)
    RETURNING
      "public".users.user_id,
      "public".users.first_name,
      "public".users.middle_name,
      "public".users.last_name,
      "public".users.suffix_name,
      "public".users.username,
      "public".users.email,
      "public".users.role_id,
      "public".users.accounting_group_id,
      "public".users.is_active`,
      [
        first_name,
        middle_name,
        last_name,
        suffix_name,
        username,
        email,
        role_id,
        accounting_group_id,
        is_active,
        date_today,
        updated_by,
        user_id
      ]
    );
  }

  async function editUserPassword(user_id, username, password) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return pool.query(
      `UPDATE users SET password=($1),updated_by=($2),updated_at=($3) WHERE username=($4)
    RETURNING 
    "public".users.user_id,
    "public".users.first_name,
    "public".users.middle_name,
    "public".users.last_name,
    "public".users.suffix_name,
    "public".users.username,
    "public".users.email,
    "public".users.role_id,
    "public".users.accounting_group_id,
    "public".users.is_active`,
      [password, user_id, date_today, username]
    );
  }

  async function resetPassword(user_id, password, updated_by) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return pool.query(
      `UPDATE users SET password=($1),updated_at=($2),updated_by=($3)
    WHERE user_id=($4) 
    RETURNING
    "public".users.user_id,
    "public".users.first_name,
    "public".users.middle_name,
    "public".users.last_name,
    "public".users.suffix_name,
    "public".users.username,
    "public".users.email,
    "public".users.role_id,
    "public".users.accounting_group_id,
    "public".users.is_active`,
      [password, date_today, updated_by, user_id]
    );
  }

  //testing
  async function getLatestUsername() {
    return pool.query(
      `SELECT username FROM users ORDER BY user_id DESC LIMIT 1`
    );
  }

  async function getLatestID() {
    return pool.query(
      `SELECT user_id FROM users ORDER BY user_id DESC LIMIT 1`
    );
  }
};
