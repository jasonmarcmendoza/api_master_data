//Moment
const moment = require("moment");
//Database connection
const client = require("../../database/index");

const axios = require("../../../axios");

const users = require("./users");

const usersDB = users({ ...axios, moment, client });

module.exports = usersDB;
