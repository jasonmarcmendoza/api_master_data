//Moment
const moment = require("moment");

//Database connection
const client = require("../../database/index");

const axios = require("../../../axios");

const signatories = require("./signatories");

const signatoriesDB = signatories({ ...axios, moment, client });

module.exports = signatoriesDB;
