module.exports = function signatoriesDB({
  axiosRequest,
  recursiveRequest,
  moment,
  client
}) {
  return Object.freeze({
    listSignatories,
    insertSignatory,
    editSignatory,
    findSignatoryById,
    listCompanySignatory
  });

  async function findSignatoryById(U_EMPLOYEE_CODE, U_COMPANY_CODE, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_SIGNATORIES?$filter=U_EMPLOYEE_CODE eq '${U_EMPLOYEE_CODE}' and U_COMPANY_CODE eq ${U_COMPANY_CODE}`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data.value;
  }

  async function listSignatories(SessionId, companies) {
    let link =
      "/b1s/v1/U_SIGNATORIES?$select=Code,U_EMPLOYEE_CODE,U_COMPANY_CODE,U_IS_ACTIVE";
    const signatories = await recursiveRequest(SessionId, link);

    let signatoryData = [];

    const companySignatories = {};
    for await (signatory of signatories) {
      if (!companySignatories[`${signatory.U_COMPANY_CODE}`]) {
        const CompanyDB = companies.find(
          company => company.ID == signatory.U_COMPANY_CODE
        ).NAME;
        let sql = `SELECT A."empID" "U_EMPLOYEE_CODE", A."firstName" "FirstName",A."lastName" "LastName",A."middleName" "MiddleName", B."name" "Position"
        FROM "${CompanyDB}"."OHEM" A
        INNER JOIN "${CompanyDB}"."OHPS" B ON A."position" = B."posID" `;

        const result = await new Promise(resolve => {
          client.connect(function(err) {
            client.exec(sql, function(err, rows) {
              resolve(rows);
            });
          });
          client.disconnect();
        });

        companySignatories[`${signatory.U_COMPANY_CODE}`] = result;
      }

      signatoryData.push({
        ...signatory,
        ...companySignatories[`${signatory.U_COMPANY_CODE}`].find(
          employee => employee.U_EMPLOYEE_CODE == signatory.U_EMPLOYEE_CODE
        )
      });
    }

    return signatoryData;
  }

  async function listCompanySignatory(SessionId, CompanyID, CompanyDB) {
    let link = `/b1s/v1/U_SIGNATORIES?$select=Code,U_EMPLOYEE_CODE,U_COMPANY_CODE,U_IS_ACTIVE&$filter=U_COMPANY_CODE eq ${CompanyID}`;
    const signatories = await recursiveRequest(SessionId, link);

    link = `/b1s/v1/$crossjoin(EmployeesInfo, EmployeePosition)?$expand=EmployeesInfo($select=EmployeeID,FirstName,LastName,MiddleName,eMail,ApplicationUserID),EmployeePosition($select=Name)&$filter=EmployeesInfo/Position eq EmployeePosition/PositionID`;

    const SL_Credentials = {
      UserName: process.env.SL_UserName,
      Password: process.env.SL_Password
    };

    await axiosRequest({
      method: "POST",
      link: `/b1s/v1/Login`,
      headers: {
        "Content-Type": "application/json"
      },
      data: {
        ...SL_Credentials,
        CompanyDB
      }
    })
      .then(res => (SessionId = res.data.SessionId))
      .catch(err => {
        if (err.code == "ECONNREFUSED") {
          throw new Error("Network Error!");
        } else if (!err.response) throw new Error(err.message);
        else if (err.response.status === 502) {
          throw new Error(err.response.statusText);
        } else if (err.response.data.error) {
          throw new Error(err.response.data.error.message.value);
        } else {
          throw new Error(err.response.data);
        }
      });

    const result = await axiosRequest({
      method: "GET",
      link,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    const finalSignatories = [];

    result.data.value.forEach(data => {
      signatories.forEach(signatory => {
        if (signatory.U_EMPLOYEE_CODE == data.EmployeesInfo.EmployeeID) {
          finalSignatories.push({
            ...signatory,
            ...data.EmployeesInfo,
            ...data.EmployeePosition
          });
        }
      });
    });

    return finalSignatories;
  }

  async function insertSignatory(signatoryInfo, SessionId) {
    const maxValue = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_SIGNATORIES/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    const result = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_SIGNATORIES`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...signatoryInfo,
        Code: maxValue.data + 1,
        Name: maxValue.data + 1,
        U_IS_ACTIVE: 1,
        U_CREATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_CREATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_UPDATED_BY: signatoryInfo.U_CREATED_BY,
        U_UPDATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }

  async function editSignatory(Code, signatoryInfo, SessionId) {
    delete signatoryInfo.Code;
    return await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_SIGNATORIES('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...signatoryInfo,
        U_UPDATED_BY: signatoryInfo.U_UPDATED_BY,
        U_UPDATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
  }
};
