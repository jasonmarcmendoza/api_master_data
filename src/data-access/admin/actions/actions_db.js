module.exports = function actionsDB({ pool, moment }) {
  return Object.freeze({
    listActions,
    findActionByName,
    insertAction,
    findActionById,
    editAction,
    //testing
    getLatestActionId
  });

  async function listActions() {
    return pool.query(`SELECT
    "public".actions.action_id,
    "public".actions.action_name,
    "public".actions.is_active,
    "public".actions.module_id,
    "public".modules.module_name
    FROM
    "public".actions
    INNER JOIN "public".modules ON "public".actions.module_id = "public".modules.module_id
    `);
  }

  async function findActionByName(action_name, module_id) {
    return pool.query(`SELECT
    "public".actions.action_id,
    "public".actions.action_name,
    "public".actions.module_id
    FROM
    "public".actions
    INNER JOIN "public".modules ON "public".actions.module_id = "public".modules.module_id
    WHERE "public".actions.action_name = '${action_name}' AND "public".modules.module_id = ${module_id}`);
  }
  async function insertAction(action_name, module_id, user_id) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return pool.query(
      `INSERT INTO actions (action_name,module_id,is_active,created_at,created_by,updated_at,updated_by)
    VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING 
    "public".actions.action_id,
    "public".actions.action_name,
    "public".actions.is_active,
    "public".actions.module_id`,
      [action_name, module_id, "TRUE", date_today, user_id, date_today, user_id]
    );
  }

  async function findActionById(action_id) {
    return pool.query(`SELECT
    "public".actions.action_id,
    "public".actions.action_name,
    "public".actions.module_id
    FROM
    "public".actions
    WHERE
    "public".actions.action_id = ${action_id}
    `);
  }

  async function editAction(
    action_id,
    action_name,
    module_id,
    is_active,
    user_id
  ) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return pool.query(
      `UPDATE actions SET action_name=($1),
    module_id=($2),is_active=($3),updated_at=($4),updated_by=($5) 
    WHERE "public".actions.action_id = ($6) RETURNING 
    "public".actions.action_id,
    "public".actions.action_name,
    "public".actions.is_active,
    "public".actions.module_id`,
      [action_name, module_id, is_active, date_today, user_id, action_id]
    );
  }

  //testing
  async function getLatestActionId() {
    return pool.query(
      `SELECT action_id FROM actions ORDER BY action_id DESC LIMIT 1`
    );
  }
};
