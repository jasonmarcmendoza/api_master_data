//Moment
const moment = require("moment");

//Database connection
const actions = require("./actions");

const axios = require("../../../axios");

const actionsDB = actions({ ...axios, moment });

module.exports = actionsDB;
