module.exports = function actionsDB({
  axiosRequest,
  recursiveRequest,
  moment
}) {
  return Object.freeze({
    listActions,
    findActionByName,
    insertAction,
    findActionById,
    editAction
    // //testing
    // getLatestActionId
  });

  async function listActions(link, SessionId) {
    const result = await recursiveRequest(SessionId, link);

    return result;
  }

  async function findActionByName(
    U_ACTION_NAME,
    U_MODULE_CODE,
    SessionId
  ) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ACTIONS?$select=Code,Name,U_MODULE_CODE,U_IS_ACTIVE&$filter=U_ACTION_NAME eq '${U_ACTION_NAME}' and U_MODULE_CODE eq '${U_MODULE_CODE}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data.value;
  }
  async function insertAction(actionInfo, SessionId) {
    const maxValue = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ACTIONS/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    const result = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_ACTIONS`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Code: maxValue.data + 1,
        Name: maxValue.data + 1,
        ...actionInfo,
        U_IS_ACTIVE: 1,
        U_CREATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_CREATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result.data;
  }

  async function findActionById(Code, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ACTIONS('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result.data;
  }

  async function editAction(
    Code,
    U_ACTION_NAME,
    U_MODULE_CODE,
    U_IS_ACTIVE,
    U_UPDATED_BY,
    SessionId
  ) {
    const result = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_ACTIONS('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        U_ACTION_NAME,
        U_MODULE_CODE,
        U_IS_ACTIVE,
        U_UPDATED_BY,
        U_UPDATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    return result;
  }

  //   //testing
  //   async function getLatestActionId() {
  //     return pool.query(
  //       `SELECT action_id FROM actions ORDER BY action_id DESC LIMIT 1`
  //     );
  //   }
};
