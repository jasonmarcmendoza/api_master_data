module.exports = function activity_logsDB({ client, moment }) {
  return Object.freeze({
    listActivityLogs
  });

  async function listActivityLogs(date_range) {
    const dateToday = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-D");
    let sql = `SELECT * FROM "${process.env.SL_CompanyDB}"."@ACTIVITY_LOGS" A
    WHERE ${
      date_range
        ? `A."U_LOG_DATE" BETWEEN '${moment(date_range.date_from)
            .tz("Asia/Manila")
            .format("YYYY-MM-D")}T00:00:00.000Z' AND '${moment(
            date_range.date_to
          )
            .tz("Asia/Manila")
            .format("YYYY-MM-D")}T23:59:59.999Z'`
        : `A."U_LOG_DATE" BETWEEN '${dateToday}T00:00:00.000Z' AND '${dateToday}T23:59:59.999Z'`
    }`;

    const result = await new Promise(resolve => {
      client.connect(function(err) {
        client.exec(sql, function(err, rows) {
          resolve(rows);
        });
      });
      client.disconnect();
    });

    return result;
  }
};
