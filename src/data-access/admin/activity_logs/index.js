//Database connection
const client = require("../../database/index");
const moment = require("moment");

const activity_logs = require("./activity_logs_db");

const activity_logsDB = activity_logs({ client, moment });

module.exports = activity_logsDB;
