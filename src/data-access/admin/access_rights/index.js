//Moment
const moment = require("moment");

//Database connection

const axios = require("../../../axios");

const accessRights = require("./access_rights");

const accessRightsDB = accessRights({ ...axios, moment });

module.exports = accessRightsDB;
