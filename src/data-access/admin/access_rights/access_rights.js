module.exports = function actionsDB({
  axiosRequest,
  recursiveRequest,
  moment
}) {
  return Object.freeze({
    // listAccessRightsByRoleId,
    // bulkCreateAccessRights,
    // bulkEditAccessRights,
    listAllAccessRightsByRoleId,
    getMaxId
  });

  //   async function listAccessRightsByRoleId(role_id) {
  //     return pool.query(`SELECT
  //           "public".access_rights.access_rights_id,
  //           "public".access_rights.role_id,
  //           "public".access_rights.action_id,
  //           "public".access_rights.is_active
  //           FROM
  //           "public".access_rights
  //           WHERE
  //           "public".access_rights.is_active = TRUE
  //           AND "public".access_rights.role_id = ${role_id}
  //           `);
  //   }

  async function getMaxId(SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_ROLE_ACCESS/$count`,
      headers: {
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }

  async function listAllAccessRightsByRoleId(SessionId, role_code) {
    const result = await recursiveRequest(
      SessionId,
      `/b1s/v1/U_ROLE_ACCESS?$select=Code,U_ROLE_CODE,U_ACTION_CODE,U_IS_ACTIVE&$filter=U_ROLE_CODE eq '${role_code}'`
    );

    return result;
  }

  //   async function bulkCreateAccessRights(actions, role_id, user_id) {
  //     let actionValues = "";
  //     const date_today = moment()
  //       .tz("Asia/Manila")
  //       .format("YYYY-MM-DD HH:mm:ss");

  //     for await (action of actions) {
  //       actionValues =
  //         actionValues +
  //         `(${action.action_id},${role_id},'TRUE','${date_today}',${user_id},'${date_today}',${user_id}),`;
  //     }
  //     actionValues = actionValues.substr(0, actionValues.length - 1);

  //     return pool.query(`INSERT INTO
  //       access_rights (action_id,role_id,is_active,created_at,created_by,updated_at,updated_by)
  //       VALUES ${actionValues}
  //       RETURNING "public".access_rights.access_rights_id,
  //       "public".access_rights.role_id,
  //       "public".access_rights.action_id,
  //       "public".access_rights.is_active`);
  //   }
  //   async function bulkEditAccessRights(actions, user_id, role_id) {
  //     let accessRights = [];
  //     const date_today = moment()
  //       .tz("Asia/Manila")
  //       .format("YYYY-MM-DD HH:mm:ss");

  //     for await (action of actions) {
  //       const { access_rights_id, action_id, is_active } = action;
  //       const result = await pool.query(
  //         `UPDATE access_rights SET action_id=($1),role_id=($2),
  //       is_active=($3),updated_at=($4),updated_by=($5) WHERE access_rights_id = ($6)
  //       RETURNING
  //       "public".access_rights.access_rights_id,
  //       "public".access_rights.role_id,
  //       "public".access_rights.action_id,
  //       "public".access_rights.is_active`,
  //         [action_id, role_id, is_active, date_today, user_id, access_rights_id]
  //       );
  //       accessRights.push(result.rows[0]);
  //     }

  //     return accessRights;
  //   }
};
