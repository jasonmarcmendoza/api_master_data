module.exports = function modulesDB({
  axiosRequest,
  recursiveRequest,
  moment
}) {
  return Object.freeze({
    listModules,
    findModuleByName,
    insertModule,
    editModule,
    findModuleById
  });

  async function listModules(SessionId, link) {
    const result = await recursiveRequest(SessionId, link);
    return result;
  }

  async function findModuleByName(moduleName, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_MODULES?$select=Code,Name,U_IS_ACTIVE&$filter=Name eq '${moduleName}'`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data.value;
  }

  async function findModuleById(Code, SessionId) {
    const result = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_MODULES('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }

  async function insertModule(Name, Code, SessionId) {
    const maxValue = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/U_MODULES/$count`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    const result = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/U_MODULES`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        Name,
        Code: maxValue.data + 1,
        U_IS_ACTIVE: 1,
        U_CREATED_BY: Code,
        U_CREATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_CREATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_UPDATED_BY: Code,
        U_UPDATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    return result.data;
  }

  async function editModule(Code, moduleInfo, SessionId) {
    // let oldVal = await axiosRequest({
    //   method: "GET",
    //   link: `/b1s/v1/U_MODULES('${Code}')`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   data: {
    //     ...moduleInfo,
    //     U_UPDATED_AT: moment()
    //       .tz("Asia/Manila")
    //       .format("YYYY-MM-DD"),
    //     U_UPDATED_TIME: moment()
    //       .tz("Asia/Manila")
    //       .format("HHmm")
    //   }
    // })
    const result = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/U_MODULES('${Code}')`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...moduleInfo,
        U_UPDATED_AT: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD"),
        U_UPDATED_TIME: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    // let newVal = await axiosRequest({
    //   method: "GET",
    //   link: `/b1s/v1/U_MODULES('${Code}')`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   data: {
    //     ...moduleInfo,
    //     U_UPDATED_AT: moment()
    //       .tz("Asia/Manila")
    //       .format("YYYY-MM-DD"),
    //     U_UPDATED_TIME: moment()
    //       .tz("Asia/Manila")
    //       .format("HHmm")
    //   }
    // })

    // oldVal = oldVal.data
    // newVal = newVal.data
    // delete oldVal['odata.metadata']
    // delete oldVal['@odata.metadata']
    // delete newVal['odata.metadata']
    // delete newVal['@odata.metadata']

    // axiosRequest({
    //   method: "POST",
    //   link: `/b1s/v1/U_ACTIVITY_LOGS`,
    //   headers: {
    //     "Content-Type": "application/json",
    //     Cookie: `B1SESSION=${SessionId};`
    //   },
    //   data: {
    //     Name: '',
    //     U_USERCODE: moduleInfo.U_UPDATED_BY,
    //     U_OPERATION: "UPDATE",
    //     U_TABLE: "MODULES",
    //     U_OLD_VALUES: JSON.stringify(oldVal),
    //     U_NEW_VALUES: JSON.stringify(newVal),
    //     U_LOG_DATE: moment()
    //       .tz("Asia/Manila")
    //       .format("YYYY-MM-DD"),
    //     U_LOG_TIME: moment()
    //       .tz("Asia/Manila")
    //       .format("HHmm")
    //   }
    // })

    return result;
  }
};
