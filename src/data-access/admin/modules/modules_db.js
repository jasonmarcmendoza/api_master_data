module.exports = function modulesDB({ pool, moment }) {
  return Object.freeze({
    listModules,
    findModuleByName,
    insertModule,
    editModule,
    findModuleById
  });

  async function listModules() {
    return pool.query(`SELECT
    "public".modules.module_id,
    "public".modules.module_name,
    "public".modules.is_active
    FROM
    "public".modules
    `);
  }

  async function findModuleByName(moduleName) {
    return pool.query(`SELECT * FROM "public".modules 
    WHERE  "public".modules.module_name = '${moduleName}'`);
  }

  async function findModuleById(findModuleById) {
    return pool.query(`SELECT * FROM "public".modules 
    WHERE  "public".modules.module_id = '${findModuleById}'`);
  }

  async function insertModule(module_name, user_id) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");
    return await pool.query(
      `INSERT INTO modules (module_name,is_active,created_at,created_by,updated_at,updated_by) VALUES ($1,$2,$3,$4,$5,$6) RETURNING *`,
      [module_name, true, date_today, user_id, date_today, user_id]
    );
  }

  async function editModule(module_id, module_name, is_active, user_id) {
    const date_today = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD HH:mm:ss");

    return await pool.query(
      `UPDATE modules SET module_name=($1),is_active=($2),updated_at=($3),updated_by=($4)
      WHERE module_id=($5) RETURNING *`,
      [module_name, is_active, date_today, user_id, module_id]
    );
  }
};
