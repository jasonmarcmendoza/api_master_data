//Moment
const moment = require("moment");

//Database connection

const modules = require("./modules");

const axios = require("../../../axios");

const modulesDB = modules({ ...axios, moment });

module.exports = modulesDB;
