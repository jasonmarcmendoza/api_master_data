const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const url = process.env.SAP_URL;

const xsjs = process.env.XSJS_URL;

const client = require("../database/index");

const usersDb = {
  selectAllUsers: async () => {
    try {
      const sql = `select * from "${process.env.DB}"."@USERS"`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneUser: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(U_USERS, U_ROLES, U_PA_STAGES)?$expand=U_USERS($select=Code),U_ROLES($select=Code,Name,U_STAGE_ID),U_PA_STAGES($select=Code,U_STAGE)&$filter=U_USERS/Code eq '${info.id}' and U_USERS/U_ROLE_CODE eq U_ROLES/Code and U_ROLES/U_STAGE_ID eq U_PA_STAGES/Code`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  selectOneUserByCompany: async ({ info }) => {
    try {
      const sql = `select "lastName" || ', ' || "firstName" as employee_name from "${info.company}"."OHEM" where "empID" = ${info.id}`;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
};

module.exports = { usersDb };
