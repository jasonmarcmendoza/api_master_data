module.exports = function groupCodeDB({
  axiosRequest,
  recursiveRequest,
  client,
  moment
}) {
  return Object.freeze({
    listPendingRequests,
    listApprovedRequests,
    listProcessedRequests,
    createRequest,
    postRequest,
    approveRequest,
    processRequest
  });

  async function listPendingRequests(date_range, SessionId) {
    const dateToday = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    let link = `/b1s/v1/BP_REQUESTS?$filter=U_STATUS eq '1' or U_STATUS eq '2'and ${
      date_range
        ? `U_DATE_CREATED ge '${date_range.date_from}' and U_DATE_CREATED le '${date_range.date_to}'`
        : `U_DATE_CREATED eq '${dateToday}'`
    }`;
    const result = await recursiveRequest(SessionId, link);
    const requestStatus = await getRequestStatus(SessionId);

    for await (request of result) {
      request.U_STATUS = requestStatus.find(
        status => status.Code == request.U_STATUS
      ).U_STATUS_NAME;

      request.RequestDetails = request.BP_REQUEST_DETAILSCollection;
      request.BPAddresses = request.BP_ADDRESSESCollection;
      request.ContactEmployees = request.BP_CONTACTSCollection;

      delete request.BP_REQUEST_DETAILSCollection;
      delete request.BP_ADDRESSESCollection;
      delete request.BP_CONTACTSCollection;
    }

    return result;
  }
  async function listApprovedRequests(date_range, SessionId) {
    const dateToday = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");
    let link = `/b1s/v1/BP_REQUESTS?$filter=U_STATUS eq '3'and ${
      date_range
        ? `U_DATE_APPROVED ge '${date_range.date_from}' and U_DATE_APPROVED le '${date_range.date_to}'`
        : `U_DATE_APPROVED eq '${dateToday}'`
    }`;
    const result = await recursiveRequest(SessionId, link);
    const requestStatus = await getRequestStatus(SessionId);

    for await (request of result) {
      request.U_STATUS = requestStatus.find(
        status => status.Code == request.U_STATUS
      ).U_STATUS_NAME;

      request.RequestDetails = request.BP_REQUEST_DETAILSCollection;
      request.BPAddresses = request.BP_ADDRESSESCollection;
      request.ContactEmployees = request.BP_CONTACTSCollection;

      delete request.BP_REQUEST_DETAILSCollection;
      delete request.BP_ADDRESSESCollection;
      delete request.BP_CONTACTSCollection;
    }
    return result;
  }
  async function listProcessedRequests(date_range, SessionId) {
    const dateToday = moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD");

    let link = `/b1s/v1/BP_REQUESTS?$filter=U_STATUS eq '4'and ${
      date_range
        ? `U_DATE_PROCESSED ge '${date_range.date_from}' and U_DATE_PROCESSED le '${date_range.date_to}'`
        : `U_DATE_PROCESSED eq '${dateToday}'`
    }`;
    const result = await recursiveRequest(SessionId, link);
    const requestStatus = await getRequestStatus(SessionId);

    for await (request of result) {
      request.U_STATUS = requestStatus.find(
        status => status.Code == request.U_STATUS
      ).U_STATUS_NAME;

      request.RequestDetails = request.BP_REQUEST_DETAILSCollection;
      request.BPAddresses = request.BP_ADDRESSESCollection;
      request.ContactEmployees = request.BP_CONTACTSCollection;

      delete request.BP_REQUEST_DETAILSCollection;
      delete request.BP_ADDRESSESCollection;
      delete request.BP_CONTACTSCollection;
    }
    return result;
  }
  async function createRequest(requestInfo, SessionId) {
    requestInfo.BP_REQUEST_DETAILSCollection = requestInfo.RequestDetails;
    requestInfo.BP_ADDRESSESCollection = requestInfo.BPAddresses;
    requestInfo.BP_CONTACTSCollection = requestInfo.ContactEmployees;
    delete requestInfo.RequestDetails;
    delete requestInfo.BPAddresses;
    delete requestInfo.ContactEmployees;

    const result = await axiosRequest({
      method: "POST",
      link: `/b1s/v1/BP_REQUESTS`,
      headers: {
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...requestInfo,
        U_STATUS: "1",
        U_DATE_CREATED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_CREATED: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_UPDATED_BY: requestInfo.U_CREATED_BY,
        U_DATE_UPDATED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_UPDATED: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
    result.data.U_STATUS = "Pending";
    result.data.RequestDetails = result.data.BP_REQUEST_DETAILSCollection;
    result.data.BPAddresses = result.data.BP_ADDRESSESCollection;
    result.data.ContactEmployees = result.data.BP_CONTACTSCollection;

    delete result.data.BP_REQUEST_DETAILSCollection;
    delete result.data.BP_ADDRESSESCollection;
    delete result.data.BP_CONTACTSCollection;
    return result.data;
  }

  async function postRequest(DocEntry, requestInfo, SessionId) {
    requestInfo.BP_REQUEST_DETAILSCollection = requestInfo.RequestDetails;
    requestInfo.BP_ADDRESSESCollection = requestInfo.BPAddresses;
    requestInfo.BP_CONTACTSCollection = requestInfo.ContactEmployees;
    delete requestInfo.RequestDetails;
    delete requestInfo.BPAddresses;
    delete requestInfo.ContactEmployees;
    delete requestInfo.DocEntry;

    requestInfo.BP_REQUEST_DETAILSCollection[0].LineId = 1;
    requestInfo.BP_ADDRESSESCollection[0].LineId = 1;
    requestInfo.BP_CONTACTSCollection[0].LineId = 1;
    return await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/BP_REQUESTS(${DocEntry})`,
      headers: {
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...requestInfo,
        U_STATUS: "2",
        U_DATE_REQUESTED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_REQUESTED: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_DATE_UPDATED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_UPDATED: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
  }

  async function approveRequest(DocEntry, requestInfo, SessionId) {
    requestInfo.BP_REQUEST_DETAILSCollection = requestInfo.RequestDetails;
    requestInfo.BP_ADDRESSESCollection = requestInfo.BPAddresses;
    requestInfo.BP_CONTACTSCollection = requestInfo.ContactEmployees;
    delete requestInfo.RequestDetails;
    delete requestInfo.BPAddresses;
    delete requestInfo.ContactEmployees;
    delete requestInfo.DocEntry;

    requestInfo.BP_REQUEST_DETAILSCollection[0].LineId = 1;
    requestInfo.BP_ADDRESSESCollection[0].LineId = 1;
    requestInfo.BP_CONTACTSCollection[0].LineId = 1;
    return await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/BP_REQUESTS(${DocEntry})`,
      headers: {
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...requestInfo,
        U_STATUS: "3",
        U_APPROVED_BY: requestInfo.U_UPDATED_BY,
        U_DATE_APPROVED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_APPROVED: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_DATE_UPDATED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_UPDATED: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });
  }
  async function processRequest(
    DocEntry,
    requestInfo,
    SessionId,
    CompanyDB,
    BPSeries
  ) {
    requestInfo.BP_REQUEST_DETAILSCollection = requestInfo.RequestDetails;
    requestInfo.BP_ADDRESSESCollection = requestInfo.BPAddresses;
    requestInfo.BP_CONTACTSCollection = requestInfo.ContactEmployees;
    delete requestInfo.RequestDetails;
    delete requestInfo.BPAddresses;
    delete requestInfo.ContactEmployees;
    delete requestInfo.DocEntry;

    requestInfo.BP_REQUEST_DETAILSCollection[0].LineId = 1;
    requestInfo.BP_ADDRESSESCollection[0].LineId = 1;
    requestInfo.BP_CONTACTSCollection[0].LineId = 1;
    const result = await axiosRequest({
      method: "PATCH",
      link: `/b1s/v1/BP_REQUESTS(${DocEntry})`,
      headers: {
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        ...requestInfo,
        U_STATUS: "4",
        U_PROCESSED_BY: requestInfo.U_UPDATED_BY,
        U_DATE_PROCESSED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_PROCESSED: moment()
          .tz("Asia/Manila")
          .format("HHmm"),
        U_DATE_UPDATED: moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD "),
        U_TIME_UPDATED: moment()
          .tz("Asia/Manila")
          .format("HHmm")
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    /**********INSERT TO DB */
    const SL_Credentials = {
      UserName: process.env.SL_UserName,
      Password: process.env.SL_Password
    };

    const data = {
      CardCode: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CODE,
      CardType: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CARD_TYPE,
      CardName: requestInfo.BP_REQUEST_DETAILSCollection[0].U_NAME,
      CardForeignName: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CARD_FNAME,
      GroupCode: requestInfo.BP_REQUEST_DETAILSCollection[0].U_GROUP_CODE,
      Currency: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CURRENCY,
      FederalTaxID: requestInfo.BP_REQUEST_DETAILSCollection[0].U_LIC_TRADE_NUM,
      PriceListNum: requestInfo.BP_REQUEST_DETAILSCollection[0].U_LIST_NUM,
      PayTermsGrpCode: requestInfo.BP_REQUEST_DETAILSCollection[0].U_GROUP_NUM,
      CreditLimit: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CREDIT_LINE,
      MaxCommitment: requestInfo.BP_REQUEST_DETAILSCollection[0].U_DEBT_LINE,
      Phone1: requestInfo.BP_REQUEST_DETAILSCollection[0].U_PHONE1,
      Phone2: requestInfo.BP_REQUEST_DETAILSCollection[0].U_PHONE2,
      Cellular: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CELLULAR,
      EmailAddress: requestInfo.BP_REQUEST_DETAILSCollection[0].U_EMAIL,
      Fax: requestInfo.BP_REQUEST_DETAILSCollection[0].U_FAX,
      Website: requestInfo.BP_REQUEST_DETAILSCollection[0].U_WEB_SITE,
      SalesPersonCode: requestInfo.BP_REQUEST_DETAILSCollection[0].U_SLP_CODE
        ? requestInfo.BP_REQUEST_DETAILSCollection[0].U_SLP_CODE
        : -1,
      FatherCard: requestInfo.BP_REQUEST_DETAILSCollection[0].U_FATHER_CARD,
      FatherType: requestInfo.BP_REQUEST_DETAILSCollection[0].U_FATHER_TYPE,
      DebitorAccount:
        requestInfo.BP_REQUEST_DETAILSCollection[0].U_DEB_PAY_ACCT,
      DownPaymentClearAct:
        requestInfo.BP_REQUEST_DETAILSCollection[0].U_DPM_CLEAR,
      RelationshipCode: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CONN_BP,
      Valid: requestInfo.BP_REQUEST_DETAILSCollection[0].U_VALID_FOR,
      ValidFrom: requestInfo.BP_REQUEST_DETAILSCollection[0].U_VALID_FROM,
      ValidTo: requestInfo.BP_REQUEST_DETAILSCollection[0].U_VALID_TO,
      Frozen: requestInfo.BP_REQUEST_DETAILSCollection[0].U_FROZEN_FOR,
      FrozenFrom: requestInfo.BP_REQUEST_DETAILSCollection[0].U_FROZEN_FROM,
      FrozenTo: requestInfo.BP_REQUEST_DETAILSCollection[0].U_FROZEN_TO,
      BPAddresses: [],
      ContactEmployees: []
    };

    for await (address of requestInfo.BP_ADDRESSESCollection) {
      data.BPAddresses.push({
        RowNum: address.U_ROW_NUM,
        BPCode: requestInfo.BP_REQUEST_DETAILSCollection[0].U_CODE,
        AddressType: address.U_ADDRESS_TYPE,
        AddressName: address.U_ADDRESS,
        AddressName2: address.U_ADDRESS2,
        AddressName3: address.U_ADDRESS3,
        Street: address.U_STREET,
        Block: address.U_BLOCK,
        City: address.U_CITY,
        ZipCode: address.U_ZIPCODE,
        County: address.U_COUNTY,
        Country: address.U_COUNTRY,
        State: address.U_STATE,
        StreetNo: address.U_STREET_NO,
        BuildingFloorRoom: address.U_BUILDING,
        TaxOffice: address.U_TAX_OFFICE,
        GlobalLocationNumber: address.U_GLB_LOC_NUM
      });
    }

    for await (contact of requestInfo.BP_CONTACTSCollection) {
      data.ContactEmployees.push({
        InternalCode: contact.U_INTERNAL_CODE,
        Name: contact.U_NAME,
        FirstName: contact.U_FIRST_NAME,
        MiddleName: contact.U_MIDDLE_NAME,
        LastName: contact.U_LAST_NAME,
        Phone1: contact.U_TEL1,
        Phone2: contact.U_TEL2,
        MobilePhone: contact.U_CELLULAR,
        E_Mail: contact.U_EMAIL,
        Fax: contact.U_FAX,
        Title: contact.U_TITLE,
        Position: contact.U_POSITION,
        Address: contact.U_ADDRESS
      });
    }

    await axiosRequest({
      method: "POST",
      link: `/b1s/v1/Login`,
      headers: {
        Cookie: `B1SESSION=${SessionId};`
      },
      data: {
        CompanyDB,
        ...SL_Credentials
      }
    }).then(res => (SessionId = res.data.SessionId));

    if (requestInfo.U_REQUEST_TYPE == "N") {
      data.Series = BPSeries.Series;
      delete data.CardCode;

      await axiosRequest({
        method: "POST",
        link: `/b1s/v1/BusinessPartners`,
        headers: {
          Cookie: `B1SESSION=${SessionId};`
        },
        data
      }).catch(err => {
        if (err.code == "ECONNREFUSED") {
          throw new Error("Network Error!");
        } else if (!err.response) throw new Error(err.message);
        else if (err.response.status === 502) {
          throw new Error(err.response.statusText);
        } else if (err.response.data.error) {
          throw new Error(err.response.data.error.message.value);
        } else {
          throw new Error(err.response.data);
        }
      });
    } else if (requestInfo.U_REQUEST_TYPE == "U") {
      await axiosRequest({
        method: "PATCH",
        link: `/b1s/v1/BusinessPartners('${data.CardCode}')`,
        headers: {
          Cookie: `B1SESSION=${SessionId};`
        },
        data
      }).catch(err => {
        if (err.code == "ECONNREFUSED") {
          throw new Error("Network Error!");
        } else if (!err.response) throw new Error(err.message);
        else if (err.response.status === 502) {
          throw new Error(err.response.statusText);
        } else if (err.response.data.error) {
          throw new Error(err.response.data.error.message.value);
        } else {
          throw new Error(err.response.data);
        }
      });
    }

    return result;
  }

  async function getRequestStatus(SessionId) {
    let link = `/b1s/v1/U_BP_REQUEST_STATUS`;
    const result = await recursiveRequest(SessionId, link);

    return result;
  }
};
