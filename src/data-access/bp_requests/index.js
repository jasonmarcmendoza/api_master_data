//Database connection
const client = require("../../data-access/bp_requests");
const axios = require("../../axios");

//Moment
const moment = require("moment-timezone");

const bp_request = require("./bp_requests");

const BPRequestsDB = bp_request({ ...axios, client, moment });

module.exports = BPRequestsDB;
