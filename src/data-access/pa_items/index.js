const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const url = process.env.SAP_URL;

const xsjs = process.env.XSJS_URL;

const client = require("../database/index");

const itemsDb = {
  selectAllItems: async ({ info }) => {
    try {
      const sql = `select a."ItemCode" as item_id, a."ItemName" as item_name, b."Price" as price
          from "${info.company}"."OITM" a
          left join "${info.company}"."ITM1" b on a."ItemCode" = b."ItemCode" where b."PriceList" = 1
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectItemDetails: async ({ info }) => {
    try {
      const sql = `select c."ItemName" as item_name , b."U_CREATED_DATE" as created_date, b."U_CREATED_TIME" as created_time,
          b."U_OLD_PRICE" as old_price , b."U_NEW_PRICE" as new_price, b."U_REQUEST_ID" as request_id
           from "${info.company}"."ITM1" a 
          left join "${process.env.SL_CompanyDB}"."@PA_PR_LOGS" b on a."ItemCode" = b."U_ITEM_ID"
          left join "${info.company}"."OITM" c on a."ItemCode" = c."ItemCode"
          where a."ItemCode" = '${info.id}' and a."PriceList" = 1 order by b."Code" DESC
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },

  selectOneItemById: async ({ info }) => {
    try {
      const sql = `
          select a."ItemName" as item_name, b."Price" as price from "${info.company}"."OITM" a 
          left join "${info.company}"."ITM1" b on a."ItemCode" = b."ItemCode"
          where a."ItemCode" = '${info.id}' and b."PriceList" = 1
          `;

      const result = await new Promise(resolve => {
        client.connect(function(err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function(err, rows) {
            resolve(rows);
          });
        });
        client.disconnect();
      });

      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
};

module.exports = { itemsDb };
