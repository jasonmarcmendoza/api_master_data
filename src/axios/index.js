const axios = require("axios");
const https = require("https");

const axiosRequest = async ({ method, link, headers, data }) => {
  return await axios({
    method: method,
    url: `${process.env.SL_URL}${link}`,
    headers,
    httpsAgent: new https.Agent({ rejectUnauthorized: false }),
    data
  });
};

const recursiveRequest = async (SessionId, link, nextLink) => {
  const arr = [];
  const result = await axiosRequest({
    method: "GET",
    link: nextLink ? nextLink : link,
    headers: {
      "Content-Type": "application/json",
      Cookie: `B1SESSION=${SessionId};`
    }
  }).catch(err => {
    if (err.response.data.error)
      throw new Error(err.response.data.error.message.value);
    else if (err.response.status === 502) {
      throw new Error(err.response.statusText);
    } else throw new Error(err.response.data);
  });

  if (!result.data) {
    throw new Error("Uknown Error.");
  }
  if (result.data["odata.nextLink"] || result.data["@odata.nextLink"]) {
    nextLink = result.data["odata.nextLink"] || result.data["@odata.nextLink"];
  } else {
    nextLink = null;
  }

  arr.push(...result.data.value);

  while (nextLink) {
    const validLink = nextLink.search("/b1s/v1");

    const test = await axiosRequest({
      method: "GET",
      link: validLink >= 0 ? nextLink : `/b1s/v1/${nextLink}`,
      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.response.data.error)
        throw new Error(err.response.data.error.message.value);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else throw new Error(err.response.data);
    });

    if (!test.data) {
      throw new Error("Uknown Error.");
    }

    if (test.data["odata.nextLink"] || test.data["@odata.nextLink"]) {
      nextLink = test.data["odata.nextLink"] || test.data["@odata.nextLink"];
    } else {
      nextLink = null;
    }
    arr.push(...test.data.value);
  }

  return arr;
};

module.exports = { axiosRequest, recursiveRequest };
