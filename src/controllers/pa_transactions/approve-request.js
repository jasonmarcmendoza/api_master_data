const approveRequest = ({ u_approveRequest }) => {
    return async function put(httpRequest) {
      try {
        // get http request to submitted data
        const { source = {}, ...info } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        info.cookie = httpRequest.SessionId;
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        // end
        const toEdit = {
          ...info,
          source,
          id: httpRequest.params.id
        };
     
        const patched = await u_approveRequest(toEdit);
        return {
          headers: {
            "Content-Type": "application/json"
          },
          status: 200,
          body: { patched }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
  
        return {
          headers: {
            "Content-Type": "application/json"
          },
          status: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = approveRequest;
  