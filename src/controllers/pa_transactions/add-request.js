const addRequest = ({ u_addRequest }) => {
    return async function post(httpRequest) {
      try {
        const { source = {}, ...info } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        info.cookie = httpRequest.SessionId;
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        
        const posted = await u_addRequest({
          ...info,
          source
        });
        return {
          headers: {
            "Content-Type": "application/json",
            "Last-Modified": new Date(posted.modifiedOn).toUTCString()
          },
          status: 201,
          body: { posted }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
  
        return {
          headers: {
            "Content-Type": "application/json"
          },
          status: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = addRequest;
  