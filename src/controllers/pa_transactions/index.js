const {
    u_addRequest,
    u_selectAllTransactions,
    u_selectOneTransaction,
    u_selectAllRequests,
    u_cancelTransaction,
    u_disapproveRequest,
    u_approveRequest,
    u_selectAllLevels,
    u_selectApprovalDetails
  } = require("../../use-cases/pa_transactions/index")
  

  const addRequest = require("./add-request");
  const selectAllTransactions = require("./select-all-transactions")
  const selectOneTransaction = require("./select-one-transaction")
  const selectAllRequests = require("./select-all-requests")
//   const cancelTransaction = require("./cancel-transaction")
  const disapproveRequest = require("./disapprove-request")
  const approveRequest = require("./approve-request")
  const selectAllLevels = require("./select-all-levels")
  const selectApprovalDetails = require("./select-approval-details")

  const c_addRequest = addRequest({ u_addRequest })
  const c_selectAllTransactions = selectAllTransactions({u_selectAllTransactions})
  const c_selectOneTransaction = selectOneTransaction({u_selectOneTransaction})
  const c_selectAllRequests = selectAllRequests({u_selectAllRequests})
//   const c_cancelTransaction = cancelTransaction({u_cancelTransaction})
  const c_disapproveRequest = disapproveRequest({u_disapproveRequest})
  const c_approveRequest = approveRequest({u_approveRequest})
  const c_selectAllLevels = selectAllLevels({u_selectAllLevels})
  const c_selectApprovalDetails = selectApprovalDetails({ u_selectApprovalDetails })

 
  const services = Object.freeze({
    c_addRequest,
    c_selectAllTransactions,
    c_selectOneTransaction,
    c_selectAllRequests,
    // c_cancelTransaction,
    c_disapproveRequest,
    c_approveRequest,
    c_selectAllLevels,
    c_selectApprovalDetails
  });
  
  module.exports = services;
  module.exports = {
    c_addRequest,
    c_selectAllTransactions,
    c_selectOneTransaction,
    c_selectAllRequests,
    // c_cancelTransaction,
    c_disapproveRequest,
    c_approveRequest,
    c_selectAllLevels,
    c_selectApprovalDetails
  };
  