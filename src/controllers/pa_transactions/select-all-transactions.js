const selectAllTransactions = ({ u_selectAllTransactions }) => {
    return async function get(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        //get the httprequest body
        const { source = {}, ...info } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        info.cookie = httpRequest.SessionId;
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        const toView = {
          ...info,
          source
        };
        const view = await u_selectAllTransactions(toView);
        return {
          headers: {
            "Content-Type": "application/json"
          },
          status: 200,
          body: { view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          status: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = selectAllTransactions;
  