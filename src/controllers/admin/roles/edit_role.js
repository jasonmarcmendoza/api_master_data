module.exports = function makeEditRole({ editRole }) {
  return async function putEditRole(httpRequest) {
    try {
      //Check token
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { user_actions } = httpRequest.body;

      if (!user_actions) {
        throw { status: 401, message: "Access denied." };
      }

      //Check for Admin module access
      if (!user_actions["Admin Module"]) {
        throw {
          status: 401,
          message: "Access denied. Not allowed to access admin module"
        };
      }

      if (
        !user_actions["Admin Module"].find(
          action => action.U_ACTION_NAME === "Edit role and access rights"
        )
      ) {
        throw {
          status: 401,
          message:
            "Access denied. Not allowed to access edit role and access rights"
        };
      }

      //remove user actions to JSON
      delete httpRequest.body.user_actions;

      const { Code } = httpRequest.params;
      const roleInfo = httpRequest.body;
      const result = await editRole(Code, roleInfo, SessionId);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 204,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
