//Use Cases
const {
  listRoles,
  addRole,
  editRole
} = require("../../../use-cases/admin/roles");

const makeListRoles = require("./list_roles");
const makeAddRole = require("./add_role");
const makeEditRole = require("./edit_role");

const getListRoles = makeListRoles({
  listRoles
});
const postAddRole = makeAddRole({ addRole });
const putEditRole = makeEditRole({ editRole });

module.exports = {
  getListRoles,
  postAddRole,
  putEditRole
};
