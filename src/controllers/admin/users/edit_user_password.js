module.exports = function makeEditUserPassword({ editUserPassword }) {
  return async function putEditUserPassword(httpRequest) {
    try {
      //Check token
      const { SessionId } = httpRequest.body;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { U_USERNAME, U_PASSWORD } = httpRequest.body;

      const result = await editUserPassword(U_USERNAME, U_PASSWORD, SessionId);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 204,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
