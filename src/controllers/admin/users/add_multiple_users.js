module.exports = function makeBulkAddUsers({
  getUsersData,

  fs
}) {
  return async function postBulkAddUsers(httpRequest) {
    try {
      const file = httpRequest.files[0];
      if (!file) {
        throw new Error("Upload failed");
      }
      if (file.filename.split(".").pop() !== "xlsx") {
        throw new Error("Invalid file.");
      }
      const usersData = await getUsersData(file.path);

      const result = await bulkCreateUsers(usersData);
      fs.unlinkSync(file.path);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
