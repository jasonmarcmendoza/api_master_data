//File System
const fs = require("fs");

//Use Cases
const {
  listUsers,
  addUser,
  editUser,
  resetUserPasword,
  editUserPassword,
  updateSAPUser
} = require("../../../use-cases/admin/users");

const makeListUsers = require("./list_users");
const makeAddUser = require("./add_user");
const makeEditUser = require("./edit_user");
const makeEditResetPassword = require("./reset_user_password");
const makeEditPassword = require("./edit_user_password");
const makeUpdateSAPUser = require("./update_sap_user");

const getListUsers = makeListUsers({
  listUsers
});
const postAddUser = makeAddUser({ addUser });
const putEditUser = makeEditUser({ editUser });
const putResetPassword = makeEditResetPassword({
  resetUserPasword
});
const putEditUserPassword = makeEditPassword({ editUserPassword });
const putUpdateSAPUser = makeUpdateSAPUser({ updateSAPUser });

module.exports = {
  getListUsers,
  postAddUser,
  putEditUser,
  putResetPassword,
  putEditUserPassword,
  putUpdateSAPUser
};
