//Use Cases
const {
  listActions,
  addAction,
  editAction
} = require("../../../use-cases/admin/actions");

const makeListActions = require("./list_actions");
const makeAddAction = require("./add_action");
const makeEditAction = require("./edit_action");

const getListActions = makeListActions({
  listActions
});
const postAddAction = makeAddAction({ addAction });
const putEditAction = makeEditAction({ editAction });

module.exports = {
  getListActions,
  postAddAction,
  putEditAction
};
