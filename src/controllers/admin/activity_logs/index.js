//Use Cases
const { listActivityLogs } = require("../../../use-cases/admin/activity_logs");

const makeListActivityLogs = require("./list_activity_logs");

const getListActivityLogs = makeListActivityLogs({
  listActivityLogs
});

module.exports = {
  getListActivityLogs
};
