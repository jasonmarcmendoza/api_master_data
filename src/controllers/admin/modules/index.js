//Use Cases
const {
  listModules,
  addModule,
  editModule
} = require("../../../use-cases/admin/modules");

const makeListModules = require("./list_modules");
const makeAddModule = require("./add_module");
const makeEditModule = require("./edit_module");

const getListModules = makeListModules({
  listModules
});
const postAddModule = makeAddModule({ addModule });
const putEditModule = makeEditModule({ editModule });

module.exports = {
  getListModules,
  postAddModule,
  putEditModule
};
