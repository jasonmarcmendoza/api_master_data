//Use Cases
const {
  listSignatories,
  addSignatory,
  editSignatory,
  listCompanySignatories
} = require("../../../use-cases/admin/signatories");

const makeListSignatories = require("./list_signatories");
const makeAddSignatory = require("./add_signatory");
const makeEditSignatory = require("./edit_signatory");
const makeListCompanySignatories = require("./list_company_signatories");

const getListSignatories = makeListSignatories({
  listSignatories
});
const postAddSignatory = makeAddSignatory({ addSignatory });
const putEditSignatory = makeEditSignatory({ editSignatory });
const getListCompanySignatories = makeListCompanySignatories({
  listCompanySignatories
});

module.exports = {
  getListSignatories,
  postAddSignatory,
  putEditSignatory,
  getListCompanySignatories
};
