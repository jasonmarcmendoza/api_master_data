//Use cases
const login = require("../../use-cases/login");

//Function
const makeLogin = require("./login");

const getLogin = makeLogin({ login });

module.exports = getLogin;
