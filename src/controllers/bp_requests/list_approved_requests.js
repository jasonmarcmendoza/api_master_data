module.exports = function makeListApprovedRequests({ listApprovedRequests }) {
  return async function getListApprovedRequests(httpRequest) {
    try {
      //Check token
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { user_actions } = httpRequest.body;

      if (!user_actions) {
        throw { status: 401, message: "Access denied." };
      }

      //Check for Admin module access
      if (!user_actions["Customers Module"]) {
        throw {
          status: 401,
          message: "Access denied. Not allowed to access customer module"
        };
      }

      if (
        !user_actions["Customers Module"].find(
          action => action.U_ACTION_NAME === "View approved requests"
        )
      ) {
        throw {
          status: 401,
          message: "Access denied. Not allowed to access view approved requests"
        };
      }

      //remove user actions to JSON
      delete httpRequest.body.user_actions;
      const date_range = httpRequest.query;

      const result = await listApprovedRequests(date_range, SessionId);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
