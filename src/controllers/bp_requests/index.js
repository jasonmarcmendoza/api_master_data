//Use Cases
const {
  listApprovedRequests,
  listPendingRequests,
  listProcessedRequests,
  createRequest,
  postRequest,
  approveRequest,
  processRequest
} = require("../../use-cases/bp_requests");

const makeListPendingRequests = require("./list_pending_requests");
const makeListApprovedRequests = require("./list_approved_requests");
const makeListProcessedRequests = require("./list_processed_requests");
const makeCreateRequest = require("./create_request");
const makePostRequest = require("./post_request");
const makeApproveRequest = require("./approve_request");
const makeProcessRequest = require("./process_request");

const getListPendingRequests = makeListPendingRequests({
  listPendingRequests
});
const getListApprovedRequests = makeListApprovedRequests({
  listApprovedRequests
});
const getListProcessedRequests = makeListProcessedRequests({
  listProcessedRequests
});
const postCreateRequest = makeCreateRequest({ createRequest });
const putPostRequest = makePostRequest({ postRequest });
const putApproveRequest = makeApproveRequest({ approveRequest });
const putProcessRequest = makeProcessRequest({ processRequest });

module.exports = {
  getListPendingRequests,
  getListApprovedRequests,
  getListProcessedRequests,
  postCreateRequest,
  putPostRequest,
  putApproveRequest,
  putProcessRequest
};
