module.exports = function makeProcessRequest({ processRequest }) {
  return async function updateRequest(httpRequest) {
    try {
      //Check token
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { user_actions } = httpRequest.body;

      if (!user_actions) {
        throw { status: 401, message: "Access denied." };
      }

      //Check for Admin module access
      if (
        !user_actions["Suppliers Module"] &&
        !user_actions["Customers Module"]
      ) {
        throw {
          status: 401,
          message:
            "Access denied. Not allowed to access supplier/customer module"
        };
      }

      if (
        !user_actions["Customers Module"].find(
          action => action.U_ACTION_NAME === "Process customer request"
        ) &&
        !user_actions["Suppliers Module"].find(
          action => action.U_ACTION_NAME === "Process supplier request"
        )
      ) {
        throw {
          status: 401,
          message: "Access denied. Not allowed to access update request"
        };
      }

      //remove user actions to JSON
      delete httpRequest.body.user_actions;

      const { DocEntry } = httpRequest.params;
      const requestInfo = httpRequest.body;

      const result = await processRequest(DocEntry, requestInfo, SessionId);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 204,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
