module.exports = function makeListProcessedRequests({ listProcessedRequests }) {
  return async function getListProcessedRequests(httpRequest) {
    try {
      //Check token
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { user_actions } = httpRequest.body;

      if (!user_actions) {
        throw { status: 401, message: "Access denied." };
      }

      //Check for Admin module access
      if (
        !user_actions["Suppliers Module"] &&
        !user_actions["Customers Module"]
      ) {
        throw {
          status: 401,
          message:
            "Access denied. Not allowed to access supplier/customer module"
        };
      }

      if (
        !user_actions["Suppliers Module"].find(
          action => action.U_ACTION_NAME === "View processed requests"
        ) &&
        !user_actions["Customers Module"].find(
          action => action.U_ACTION_NAME === "View processed requests"
        )
      ) {
        throw {
          status: 401,
          message:
            "Access denied. Not allowed to access view processed requests"
        };
      }

      //remove user actions to JSON
      delete httpRequest.body.user_actions;
      const date_range = httpRequest.query;

      const result = await listProcessedRequests(date_range, SessionId);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
