const {
    u_selectAllReports
  } = require("../../use-cases/pa_reports/index")
  

  const selectAllReports = require("./select-all-reports");

  const c_selectAllReports = selectAllReports({ u_selectAllReports })


 
  const services = Object.freeze({
    c_selectAllReports
  });
  
  module.exports = services;
  module.exports = {
    c_selectAllReports
  };
  