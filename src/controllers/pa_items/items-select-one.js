const selectOneItem = ({ u_selectOneItem }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      //get the httprequest body
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      info.cookie = httpRequest.SessionId;
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const toView = {
        ...info,
        source,
        id: httpRequest.params.id,
      };
      // end here; to retrieve id
      const view = await u_selectOneItem(toView);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        status: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = selectOneItem;
