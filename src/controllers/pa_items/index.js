const {
    u_selectAllItems,
    u_selectOneItem
  } = require("../../use-cases/pa_items/index");
  

  const selectAllItems = require("./items-select-all");
  const selectOneItem = require("./items-select-one")


  const c_selectAllItems = selectAllItems({ u_selectAllItems });
  const c_selectOneItem = selectOneItem({ u_selectOneItem })

 
  const services = Object.freeze({
    c_selectAllItems,
    c_selectOneItem
  });
  
  module.exports = services;
  module.exports = {
    c_selectAllItems,
    c_selectOneItem
  };
  