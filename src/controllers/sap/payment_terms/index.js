//Use Cases
const { listPaymentTerms } = require("../../../use-cases/sap/payment_terms");

const makeListPaymentTerms = require("./list_payment_terms");

const getListPaymentTerms = makeListPaymentTerms({
  listPaymentTerms
});

module.exports = {
  getListPaymentTerms
};
