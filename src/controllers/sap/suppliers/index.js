//Use Cases
const { listSuppliers } = require("../../../use-cases/sap/suppliers");

const makeListSuppliers = require("./list_suppliers");

const getListSuppliers = makeListSuppliers({
  listSuppliers
});

module.exports = {
  getListSuppliers
};
