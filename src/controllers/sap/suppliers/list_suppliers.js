module.exports = function makeListSuppliers({ listSuppliers }) {
  return async function getListSuppliers(httpRequest) {
    try {
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }
      const { CompanyDB } = httpRequest.params;
      if (!CompanyDB) {
        throw new Error("Company DB must be provided");
      }

      const result = await listSuppliers(CompanyDB, SessionId);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 201,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
