//Use Cases
const { listCompanies } = require("../../../use-cases/sap/companies");

const makeListCompanies = require("./list_companies");

const getListCompanies = makeListCompanies({
  listCompanies
});

module.exports = {
  getListCompanies
};
