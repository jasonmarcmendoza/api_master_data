//Use Cases
const { listCountries } = require("../../../use-cases/sap/countries");

const makeListCountries = require("./list_countries");

const getListCountries = makeListCountries({
  listCountries
});

module.exports = {
  getListCountries
};
