//Use Cases
const { listStates } = require("../../../use-cases/sap/states");

const makeListStates = require("./list_states");

const getListStates = makeListStates({
  listStates
});

module.exports = {
  getListStates
};
