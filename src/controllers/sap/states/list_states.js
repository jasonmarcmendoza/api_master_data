module.exports = function makeListStates({ listStates }) {
  return async function getListStates(httpRequest) {
    try {
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { CompanyDB, Country } = httpRequest.query;

      const result = await listStates(SessionId, CompanyDB, Country);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
