//Use Cases
const { listBPGroups } = require("../../../use-cases/sap/bp_groups");

const makeListBPGroups = require("./list_bp_groups");

const getListBPGroups = makeListBPGroups({
  listBPGroups
});

module.exports = {
  getListBPGroups
};
