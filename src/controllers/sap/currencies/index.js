//Use Cases
const { listCurrencies } = require("../../../use-cases/sap/currencies");

const makeListCurrencies = require("./list_currencies");

const getListCurrencies = makeListCurrencies({
  listCurrencies
});

module.exports = {
  getListCurrencies
};
