//Use Cases
const { getEmployeeInfo } = require("../../../use-cases/sap/employees");

const makeEmployeeInfo = require("./get_employee_info");

const getEmployeeInfoByID = makeEmployeeInfo({
  getEmployeeInfo
});

module.exports = {
  getEmployeeInfoByID
};
