//Use Cases
const {
  listChartOfAccounts
} = require("../../../use-cases/sap/chart_of_accounts");

const makeListChartOfAccounts = require("./list_chart_of_accounts");

const getListChartOfAccounts = makeListChartOfAccounts({
  listChartOfAccounts
});

module.exports = {
  getListChartOfAccounts
};
