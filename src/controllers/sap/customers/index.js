//Use Cases
const { listCustomers } = require("../../../use-cases/sap/customers");

const makeListCustomers = require("./list_customers");

const getListCustomers = makeListCustomers({
  listCustomers
});

module.exports = {
  getListCustomers
};
