module.exports = function makeFetchPriceList({ fetchPriceList }) {
  return async function getPriceList(httpRequest) {
    try {
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { CompanyDB } = httpRequest.params;

      const result = await fetchPriceList(SessionId, CompanyDB);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
