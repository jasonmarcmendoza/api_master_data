//Use Cases
const { fetchPriceList } = require("../../../use-cases/sap/price_list");

const makeFetchPriceList = require("./fetch_price_list");

const getPriceList = makeFetchPriceList({
  fetchPriceList
});

module.exports = {
  getPriceList
};
