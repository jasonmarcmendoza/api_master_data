//Use Cases
const {
  listSalesEmployees
} = require("../../../use-cases/sap/sales_employees");

const makeListSalesEmployees = require("./list_sales_employees");

const getListSalesEmployees = makeListSalesEmployees({
  listSalesEmployees
});

module.exports = {
  getListSalesEmployees
};
