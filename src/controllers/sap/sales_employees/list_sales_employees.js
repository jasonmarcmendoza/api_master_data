module.exports = function makeListSalesEmployees({ listSalesEmployees }) {
  return async function getListSalesEmployees(httpRequest) {
    try {
      const SessionId = httpRequest.SessionId;
      if (!SessionId) {
        throw { status: 403, message: "Forbidden." };
      }

      const { CompanyDB } = httpRequest.params;

      const result = await listSalesEmployees(SessionId, CompanyDB);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: 200,
        body: result
      };
    } catch (e) {
      return {
        headers: {
          "Content-Type": "application/json"
        },
        status: e.status ? e.status : 400,
        body: { errorMsg: e.message }
      };
    }
  };
};
