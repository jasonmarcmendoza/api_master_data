const { itemsDb } = require("../../data-access/pa_items/index")
const { transactionsDb } = require("../../data-access/pa_transactions/index")
const { usersDb } = require("../../data-access/pa_users/index")

const selectAllItems = require("./items-select-all")
const selectOneItem = require("./items-select-one")

const u_selectAllItems = selectAllItems({ itemsDb, transactionsDb })
const u_selectOneItem = selectOneItem({ itemsDb, transactionsDb, usersDb })

const services = Object.freeze({
    u_selectAllItems,
    u_selectOneItem
  })
  
  module.exports = services
  module.exports = {
    u_selectAllItems,
    u_selectOneItem
  };
  