const selectOneItem = ({ itemsDb, transactionsDb, usersDb }) => {
    return async function get(info) {

      if(!info.company_id){
        throw new Error(`Please input company`)
      }

      const companyDetails = await transactionsDb.selectOneCompany({info: {id: info.company_id}})


      info.company = companyDetails[0].NAME

      const itemDetails = await itemsDb.selectItemDetails({info: {
        id: info.id,
        company: info.company
      }})

      for(let i = 0; i < itemDetails.length; i++){
    
      

        const requesterInformation = await transactionsDb.selectRequesterByRequestId({info: {
          id: itemDetails[i].REQUEST_ID
        }})

        

        const finalApproverInformation = await transactionsDb.selectFinalApproverByRequestId({info: {
          id: itemDetails[i].REQUEST_ID
        }})

        

        if(requesterInformation.length !== 0 && finalApproverInformation.length !== 0){
       
        const requesterCompanyDetails = await transactionsDb.selectOneCompany({info: {id: requesterInformation[0].COMPANY_CODE}})
        

        const requesterCompany = requesterCompanyDetails[0].NAME

        const fapproverCompanyDetails = await transactionsDb.selectOneCompany({info: {id: finalApproverInformation[0].COMPANY_CODE}})

        const fapproverCompany = fapproverCompanyDetails[0].NAME

        
        const requesterEmployeeDetails = await usersDb.selectOneUserByCompany({info: {
          company: requesterCompany,
          id: requesterInformation[0].EMPLOYEE_CODE
        }})

        
        const fapproverEmployeeDetails = await usersDb.selectOneUserByCompany({info: {
          company: fapproverCompany,
          id: finalApproverInformation[0].EMPLOYEE_CODE
        }})


        itemDetails[i].REQUESTER = requesterEmployeeDetails[0].EMPLOYEE_NAME
        itemDetails[i].APPROVER = fapproverEmployeeDetails[0].EMPLOYEE_NAME
      }

      }
   

      return itemDetails
  }
  };
  
  module.exports = selectOneItem;
  