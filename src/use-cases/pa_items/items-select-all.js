const selectAllItems = ({ itemsDb, transactionsDb }) => {
    return async function get(info) {


      if(!info.company_id){
        throw new Error(`Please input comany`)
      }

      const companyDetails = await transactionsDb.selectOneCompany({info: {id: info.company_id}})


      info.company = companyDetails[0].NAME


      const items = await itemsDb.selectAllItems({info})

    
      return items

  }
  };
  
  module.exports = selectAllItems;
  