const makeLogin = require("./login");
const { axiosRequest } = require("../../axios");

//Database
const userDB = require("../../data-access/admin/users/index");
const companyDB = require("../../data-access/sap/companies/index");

const login = makeLogin({
  userDB,
  axiosRequest,
  companyDB
});

module.exports = login;
