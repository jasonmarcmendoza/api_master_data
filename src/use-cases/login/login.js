module.exports = function makeLogin({
  userDB,

  createToken,
  axiosRequest,
  companyDB
}) {
  return async function makeLoginUser({ username, password }) {
    let SessionId;
    let SL_Credentials = {
      CompanyDB: process.env.SL_CompanyDB,
      Password: process.env.SL_Password,
      UserName: process.env.SL_UserName
    };
    if (!username) {
      throw new Error("Employee code is required.");
    }
    if (!password) {
      throw new Error("Password is required.");
    }

    await axiosRequest({
      method: "POST",
      link: "/b1s/v1/Login",
      headers: {
        "Content-Type": "application/json"
      },
      data: {
        ...SL_Credentials
      }
    })
      .then(res => (SessionId = res.data.SessionId))
      .catch(err => {
        if (err.code == "ECONNREFUSED") {
          throw new Error("Network Error!");
        } else if (!err.response) throw new Error(err.message);
        else if (err.response.status === 502) {
          throw new Error(err.response.statusText);
        } else if (err.response.data.error) {
          throw new Error(err.response.data.error.message.value);
        } else {
          throw new Error(err.response.data);
        }
      });

    const user = await axiosRequest({
      method: "GET",
      link: `/b1s/v1/sml.svc/USER_DETAILS?$filter=U_USERNAME eq '${username}'`,

      headers: {
        "Content-Type": "application/json",
        Cookie: `B1SESSION=${SessionId};`
      }
    }).catch(err => {
      if (err.code == "ECONNREFUSED") {
        throw new Error("Network Error!");
      } else if (!err.response) throw new Error(err.message);
      else if (err.response.status === 502) {
        throw new Error(err.response.statusText);
      } else if (err.response.data.error) {
        throw new Error(err.response.data.error.message.value);
      } else {
        throw new Error(err.response.data);
      }
    });

    if (user.data.value.length < 1) {
      throw new Error("User does not exist.");
    }

    if (!user.data.value[0].USER_STATUS) {
      throw new Error("Account is inactive.");
    }
    if (!user.data.value[0].ROLE_STATUS) {
      throw new Error("User role is inactive.");
    }
    if (user.data.value[0].U_IS_SAP_USER === 1) {
      await axiosRequest({
        method: "POST",
        link: "/b1s/v1/Login",
        headers: {
          "Content-Type": "application/json"
        },
        data: {
          CompanyDB: SL_Credentials.CompanyDB,
          Password: password,
          UserName: username
        }
      })
        .then(res => (SessionId = res.data.SessionId))
        .catch(err => {
          if (err.code == "ECONNREFUSED") {
            throw new Error("Network Error!");
          } else if (!err.response) throw new Error(err.message);
          else if (err.response.status === 502) {
            throw new Error(err.response.statusText);
          } else if (err.response.data.error) {
            throw new Error(err.response.data.error.message.value);
          } else {
            throw new Error(err.response.data);
          }
        });
    } else {
      if (user.data.value[0].U_PASSWORD != password) {
        throw new Error("Incorrect password.");
      }
    }

    const user_details = await getUserDetails(
      user.data.value[0].U_EMPLOYEE_CODE,
      user.data.value[0].U_COMPANY_CODE,
      SessionId,
      companyDB
    );
    if (user_details.length < 1) {
      throw new Error("User details not found.");
    }

    //remove Username and password from result
    delete user.data.value[0].U_EMPLOYEE_CODE;
    delete user.data.value[0].U_PASSWORD;

    const result = await getAllActions(
      axiosRequest,
      user.data.value[0].ROLE_CODE,
      SessionId
    );

    if (result.length < 1) {
      throw new Error("User role does not have access.");
    }

    let user_actions = result.reduce((prev, curr) => {
      prev[curr.MODULE_NAME] = [...(prev[curr.MODULE_NAME] || []), curr];
      return prev;
    }, {});

    // for await (modules of result.modules) {
    //   user_actions[modules.Name] = result.actions.filter(
    //     action => action.U_MODULE_CODE === modules.Code
    //   );
    // }

    return {
      SessionId,
      user_details: {
        Code: user.data.value[0].USER_CODE,
        U_IS_ACTIVE: user.data.value[0].USER_STATUS,
        U_IS_SAP_USER: user.data.value[0].U_IS_SAP_USER,
        FirstName: user_details[0].FirstName,
        MiddleName: user_details[0].MiddleName,
        LastName: user_details[0].LastName,
        U_USERNAME: user.data.value[0].U_USERNAME,
        U_COMPANY_CODE: user.data.value[0].U_COMPANY_CODE
      },
      user_role: {
        Code: user.data.value[0].ROLE_CODE,
        Name: user.data.value[0].ROLE_NAME,
        U_STAGE_ID: user.data.value[0].U_STAGE_ID,
        U_IS_ACTIVE: user.data.value[0].ROLE_STATUS
      },
      user_actions
    };
  };
};

const getUserDetails = async (EmployeeID, Company, SessionId, companyDB) => {
  const client = require("../../data-access/database/index");
  const companyList = await companyDB.listCompanies(SessionId);

  Company = companyList.filter(company => company.ID == Company)[0].NAME;

  const sql = `SELECT 
  A."firstName" "FirstName",
  A."middleName" "MiddleName",
  A."lastName" "LastName"
   FROM "${Company}"."OHEM" A 
   WHERE A."empID" = '${EmployeeID}' `;

  const result = await new Promise(resolve => {
    client.connect(function(err) {
      client.exec(sql, function(err, rows) {
        resolve(rows);
      });
    });
    client.disconnect();
  });

  return result;
};

const getAllActions = async (axiosRequest, role_code, SessionId, nextLink) => {
  let actions = [];
  let modules = [];
  const result = await axiosRequest({
    method: "GET",
    link: nextLink
      ? "/b1s/v1/sml.svc/" + nextLink
      : `/b1s/v1/sml.svc/LIST_ROLE_ACTIONS?$filter=ROLE_CODE eq '${role_code}'`,
    headers: {
      "Content-Type": "application/json",
      Cookie: `B1SESSION=${SessionId};`
    }
  }).catch(err => {
    if (err.code == "ECONNREFUSED") {
      throw new Error("Network Error!");
    } else if (!err.response) throw new Error(err.message);
    else if (err.response.status === 502) {
      throw new Error(err.response.statusText);
    } else if (err.response.data.error) {
      throw new Error(err.response.data.error.message.value);
    } else {
      throw new Error(err.response.data);
    }
  });

  actions.push(...result.data.value);

  if (result.data["@odata.nextLink"]) {
    const res = await getAllActions(
      axiosRequest,
      role_code,
      SessionId,
      result.data["@odata.nextLink"]
    );
    actions.push(...res);
  }

  return actions;
};
