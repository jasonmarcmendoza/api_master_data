//Database
const rolesDB = require("../../../data-access/admin/roles");
const accessRightsDB = require("../../../data-access/admin/access_rights");

const moment = require("moment-timezone");

const makeListRoles = require("./list_roles");
const makeAddRole = require("./add_role");
const makeEditRole = require("./edit_role");

const listRoles = makeListRoles({ rolesDB, accessRightsDB });
const addRole = makeAddRole({ rolesDB, accessRightsDB, moment });
const editRole = makeEditRole({ rolesDB, accessRightsDB, moment });

module.exports = {
  listRoles,
  addRole,
  editRole
};
