//Entities
const makeRole = require("../../../entities/admin/roles");
const makeAccessRights = require("../../../entities/admin/access_rights");

module.exports = function makeAddrole({ rolesDB, accessRightsDB, moment }) {
  return async function addRole(roleInfo, SessionId) {
    await makeRole.addRole(roleInfo);
    const { Name, U_CREATED_BY, actions } = roleInfo;

    if (!actions) {
      throw new Error("Actions must be provided.");
    }
    if (!Array.isArray(actions)) {
      throw new Error("Invalid actions.");
    }

    await makeAccessRights.addAccessRights(Code, actions, U_UPDATED_BY);

    const exists = await rolesDB.findRoleByName(Name, SessionId);
    if (exists.length > 0) {
      throw new Error("Role already exists.");
    }

    let accessMaxValue = await accessRightsDB.getMaxId(SessionId);
    let roleMaxValue = await rolesDB.getRoleMaxID(SessionId);

    let batch = null;
    batch = `--a\n\nContent-Type: multipart/mixed;boundary=b\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\n\nPOST /b1s/v1/U_ROLES\n\n{\n\t"Code":"${roleMaxValue +
      1}",\n\t"Name":"${Name}",\n\t"U_IS_ACTIVE":1,\n\t"U_CREATED_BY":"${U_CREATED_BY}",\n\t"U_CREATED_AT":"${moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD")}",\n\t"U_CREATED_TIME":"${moment()
      .tz("Asia/Manila")
      .format(
        "HHmm"
      )}",\n\t"U_UPDATED_BY":"${U_CREATED_BY}",\n\t"U_UPDATED_AT":"${moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD")}",\n\t"U_UPDATED_TIME":"${moment()
      .tz("Asia/Manila")
      .format("HHmm")}"\n}`;

    for await (access of actions) {
      accessMaxValue++;

      batch =
        batch +
        `\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\n\nPOST /b1s/v1/U_ROLE_ACCESS\n\n{\n\t"Code":"${accessMaxValue}",\n\t"Name":"${accessMaxValue}",\n\t"U_ROLE_CODE":"${roleMaxValue +
          1}",\n\t"U_ACTION_CODE":"${
          access.Code
        }",\n\t"U_IS_ACTIVE":1,\n\t"U_CREATED_BY":"${U_CREATED_BY}",\n\t"U_CREATED_AT":"${moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD")}",\n\t"U_CREATED_TIME":"${moment()
          .tz("Asia/Manila")
          .format(
            "HHmm"
          )}",\n\t"U_UPDATED_BY":"${U_CREATED_BY}",\n\t"U_UPDATED_AT":"${moment()
          .tz("Asia/Manila")
          .format("YYYY-MM-DD")}",\n\t"U_UPDATED_TIME":"${moment()
          .tz("Asia/Manila")
          .format("HHmm")}"\n}`;
    }

    batch = batch + "\n\n--b--\n--a--";

    const result = await rolesDB.insertRole(SessionId, batch);
    if (result.search("Bad Request") >= 0) {
      throw new Error("Insert failed.");
    }

    return;
  };
};
