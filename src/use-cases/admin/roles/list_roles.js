module.exports = function makeListRoles({ rolesDB, accessRightsDB }) {
  return async function listRoles(SessionId) {
    const link = `/b1s/v1/U_ROLES?$select=Code,Name,U_STAGE_ID,U_IS_ACTIVE`;

    // `$crossjoin(U_ROLES,U_ROLE_ACCESS,U_ACTIONS)?$expand=U_ROLES($select=Code,Name,U_IS_ACTIVE),U_ROLE_ACCESS($select=Code,U_ACTION_CODE,U_ROLE_CODE),U_ACTIONS($select=Code,Name,U_MODULE_CODE,U_IS_ACTIVE)&$filter=U_ROLES/Code eq U_ROLE_ACCESS/U_ROLE_CODE and U_ROLE_ACCESS/U_ACTION_CODE eq U_ACTIONS/Code`;

    const result = await rolesDB.listRoles(SessionId, link);

    if (result.length < 1) {
      return { message: "No roles found." };
    }

    // console.log(result);

    // const roles = new Array();

    // for await (role of result) {
    //   if (
    //     roles.find(role1 => role1.Code === role.U_BFI_CRA_ROLES.Code) &&
    //     !roles[
    //       roles.findIndex(role1 => role1.Code === role.U_BFI_CRA_ROLES.Code)
    //     ].actions.find(action => action.Code === role.U_BFI_CRA_ACTIONS.Code)
    //   ) {
    //     roles[
    //       roles.findIndex(role1 => role1.Code === role.U_BFI_CRA_ROLES.Code)
    //     ].actions.push(role.U_BFI_CRA_ACTIONS);
    //   } else {
    //     roles.push({
    //       ...role.U_BFI_CRA_ROLES,
    //       actions: [role.U_BFI_CRA_ACTIONS]
    //     });
    //   }
    // }

    for await (role of result) {
      const accessRights = await accessRightsDB.listAllAccessRightsByRoleId(
        SessionId,
        role.Code
      );

      role.actions = accessRights;
    }

    //   const roleActions = accessRights.rows.reduce((unique, item) => {
    //     return unique.find(action => action.action_id === item.action_id) ? unique : [...unique, item]
    //   }, [])

    //   role.actions = roleActions
    // }

    const data = {
      message: "Successfull..",
      roles: result
    };

    return data;
  };
};
