//Entities
const makeRole = require("../../../entities/admin/roles");
const makeAccessRights = require("../../../entities/admin/access_rights");
module.exports = function makeEditRole({ rolesDB, accessRightsDB, moment }) {
  return async function editRole(Code, roleInfo, SessionId) {
    await makeRole.editRole(Code, roleInfo);
    const { Name, U_UPDATED_BY, U_IS_ACTIVE, actions } = roleInfo;

    const exists = await rolesDB.findRoleById(Code, SessionId);
    if (!exists) {
      throw new Error("Role does not exist.");
    }
    const nameExists = await rolesDB.findRoleByName(exists.Name, SessionId);
    if (
      nameExists[0] &&
      nameExists[0].Code != Code &&
      nameExists[0].Name == Name
    ) {
      throw new Error("Role name already exists");
    }

    const roleAccess = await accessRightsDB.listAllAccessRightsByRoleId(
      SessionId,
      Code
    );

    let existingAccess = [];
    let newAccess = [];
    for await (action of actions) {
      const exists = roleAccess.find(access => access.Code === action.Code);
      if (exists) {
        existingAccess.push(action);
      } else {
        newAccess.push(action);
      }
    }

    await makeAccessRights.addAccessRights(Code, newAccess, U_UPDATED_BY);
    await makeAccessRights.editAccessRights(existingAccess, U_UPDATED_BY);

    let batch = null;
    batch = `--a\n\nContent-Type: multipart/mixed;boundary=b\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\n\nPATCH /b1s/v1/U_ROLES('${Code}')\n\n{\n\t"Name":"${Name}",\n\t"U_IS_ACTIVE":${U_IS_ACTIVE},\n\t"U_UPDATED_BY":"${U_UPDATED_BY}",\n\t"U_UPDATED_AT":"${moment()
      .tz("Asia/Manila")
      .format("YYYY-MM-DD")}",\n\t"U_UPDATED_TIME":"${moment()
      .tz("Asia/Manila")
      .format("HHmm")}"\n}`;

    if (existingAccess.length) {
      for await (access of existingAccess) {
        batch =
          batch +
          `\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\n\nPATCH /b1s/v1/U_ROLE_ACCESS('${
            access.Code
          }')\n\n{\n\t"U_IS_ACTIVE":${
            access.U_IS_ACTIVE
          },\n\t"U_UPDATED_BY":"${U_UPDATED_BY}",\n\t"U_UPDATED_AT":"${moment()
            .tz("Asia/Manila")
            .format("YYYY-MM-DD")}",\n\t"U_UPDATED_TIME":"${moment()
            .tz("Asia/Manila")
            .format("HHmm")}"\n}`;
      }
    }
    if (newAccess.length) {
      let maxValue = await accessRightsDB.getMaxId(SessionId);

      for await (access of newAccess) {
        maxValue++;

        batch =
          batch +
          `\n\n--b\nContent-Type:application/http\nContent-Transfer-Encoding:binary\n\nPOST /b1s/v1/U_ROLE_ACCESS\n\n{\n\t"Code":"${maxValue}",\n\t"Name":"${maxValue}",\n\t"U_ROLE_CODE":"${Code}",\n\t"U_ACTION_CODE":"${
            access.U_ACTION_CODE
          }",\n\t"U_IS_ACTIVE":${
            access.U_IS_ACTIVE
          },\n\t"U_CREATED_BY":"${U_UPDATED_BY}",\n\t"U_CREATED_AT":"${moment()
            .tz("Asia/Manila")
            .format("YYYY-MM-DD")}",\n\t"U_CREATED_TIME":"${moment()
            .tz("Asia/Manila")
            .format("HHmm")}"\n}`;
      }
    }

    batch = batch + "\n\n--b--\n--a--";

    const result = await rolesDB.editRole(SessionId, batch);
    if (result.search("Bad Request") >= 0) {
      throw new Error("Update failed.");
    }

    // if (result.rowCount < 1) {
    //   throw new Error("Update failed.");
    // }

    // const actionsResult = await accessRightsDB.bulkEditAccessRights(
    //   existingAccess,
    //   user_id,
    //   role_id
    // );

    // result.rows[0].actions = actionsResult;

    // if (newAccess.length > 0) {
    //   const newActionsResult = await accessRightsDB.bulkCreateAccessRights(
    //     newAccess,
    //     role_id,
    //     user_id
    //   );
    //   result.rows[0].actions.push(...newActionsResult.rows);
    // }

    // const data = {
    //   message: "Successfull..",
    //   results: result.rows[0]
    // };

    return;
  };
};
