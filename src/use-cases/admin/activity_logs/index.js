//Moment
const moment = require("moment");

//Database
const activity_logsDB = require("../../../data-access/admin/activity_logs");
const accessRightsDB = require("../../../data-access/admin/access_rights");
const usersDB = require("../../../data-access/admin/users");
const companyDB = require("../../../data-access/sap/companies");
const employeeDB = require("../../../data-access/sap/employees");

const makeListActivityLogs = require("./list_activity_logs");

const listActivityLogs = makeListActivityLogs({
  activity_logsDB,
  accessRightsDB,
  usersDB,
  moment,
  companyDB,
  employeeDB
});

module.exports = {
  listActivityLogs
};
