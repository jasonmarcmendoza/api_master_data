module.exports = function makeListAcitivyLogs({
  activity_logsDB,
  transactionDocumentsDB,
  accessRightsDB,
  usersDB,
  moment,
  companyDB,
  employeeDB
}) {
  return async function listActivityLogs(date_range, SessionId) {
    let query = false;

    if (
      (date_range.date_from && !date_range.date_to) ||
      (date_range.date_to && !date_range.date_from)
    ) {
      throw new Error("Invalid date range.");
    } else if (date_range.date_from && date_range.date_to) {
      query = true;
    }

    const companies = await companyDB.listCompanies(SessionId);

    const result = await activity_logsDB.listActivityLogs(
      query ? date_range : null
    );
    if (result.length < 1) {
      return { message: "No activity logs found." };
    }

    for await (data of result) {
      const user = await usersDB.findUserById(data.U_USERCODE, SessionId);
      const employee = await employeeDB.getEmployeeInfoByID(
        user.U_EMPLOYEE_CODE,
        companies.find(comapny => comapny.ID == user.U_COMPANY_CODE).NAME
      );
      let employeeName = `${employee.FirstName} ${employee.MiddleName} ${employee.LastName}`;
      data.Employee = employeeName;

      if (data.U_OLD_VALUES) {
        data.U_OLD_VALUES = JSON.parse(data.U_OLD_VALUES.toString());
      }
      if (data.U_NEW_VALUES) {
        data.U_NEW_VALUES = JSON.parse(data.U_NEW_VALUES.toString());
      }
      data.U_LOG_DATE = moment(data.U_LOG_DATE)
        .tz("Asia/Manila")
        .format("MMM D, YYYY hh:mm A");
    }

    // for await (data of result.rows) {
    //   const user = await usersDB.findUserById(data.user_id)
    //   let fullName

    //   //decypt user details from user id

    //   if (user.rows.length) {
    //     fullName = encryption.decrypt(user.rows[0].first_name) + ' '

    //     if (user.rows[0].middle_name)
    //       fullName = fullName + encryption.decrypt(user.rows[0].middle_name) + ' '

    //     fullName = fullName + encryption.decrypt(user.rows[0].last_name)

    //     if (user.rows[0].suffix_name)
    //       fullName = fullName + ' ' + encryption.decrypt(user.rows[0].suffix_name)

    //     data.userFullName = fullName

    //   }

    //   if (data.table_name === "transactions") {
    //     if (data.old_values) {
    //       const oldVal = JSON.parse(data.old_values);

    //       const documents = await transactionDocumentsDB.getTransactionDocuments(
    //         oldVal.reference_no ? oldVal.reference_no : oldVal.counter_receipt_no
    //       );

    //       for await (row of documents.rows) {
    //         row.inv_date = moment(row.inv_date)
    //           .tz("Asia/Manila")
    //           .format("YYYY-MM-DD");
    //       }
    //       oldVal.documents = documents.rows;
    //       oldVal.date = moment(oldVal.date)
    //         .tz("Asia/Manila")
    //         .format("YYYY-MM-DD");

    //       data.old_values = oldVal;
    //     }

    //     const newVal = JSON.parse(data.new_values);

    //     const documents = await transactionDocumentsDB.getTransactionDocuments(
    //       newVal.reference_no ? newVal.reference_no : newVal.counter_receipt_no
    //     );

    //     for await (row of documents.rows) {
    //       row.inv_date = moment(row.inv_date)
    //         .tz("Asia/Manila")
    //         .format("YYYY-MM-DD");
    //     }
    //     newVal.documents = documents.rows;
    //     newVal.date = moment(newVal.date)
    //       .tz("Asia/Manila")
    //       .format("YYYY-MM-DD");

    //     data.new_values = newVal;
    //   } else if (data.table === "roles") {
    //     const accessRights = await accessRightsDB.listAccessRightsByRoleId(
    //       data.role_id
    //     );
    //     data.actions = accessRights.rows;
    //   } else {
    //     //decrypt user details if exists
    //     if (data.old_values) {
    //       const oldVal = JSON.parse(data.old_values);

    //       if (oldVal.first_name)
    //         oldVal.first_name = encryption.decrypt(oldVal.first_name)

    //       if (oldVal.middle_name)
    //         oldVal.middle_name = encryption.decrypt(oldVal.middle_name)
    //       if (oldVal.last_name)
    //         oldVal.last_name = encryption.decrypt(oldVal.last_name)
    //       if (oldVal.suffix_name)
    //         oldVal.suffix_name = encryption.decrypt(oldVal.suffix_name)

    //       data.old_values = oldVal
    //     }

    //     const newVal = JSON.parse(data.new_values);

    //     if (newVal.first_name)
    //       newVal.first_name = encryption.decrypt(newVal.first_name)
    //     if (newVal.middle_name)
    //       newVal.middle_name = encryption.decrypt(newVal.middle_name)
    //     if (newVal.last_name)
    //       newVal.last_name = encryption.decrypt(newVal.last_name)
    //     if (newVal.suffix_name)
    //       newVal.suffix_name = encryption.decrypt(newVal.suffix_name)

    //     data.new_values = newVal
    //   }

    //   data.date = moment(data.date)
    //     .tz("Asia/Manila")
    //     .format("YYYY-MM-DD");
    // }

    const response = {
      result: "Success..",
      activity_logs: result
    };
    return response;
  };
};
