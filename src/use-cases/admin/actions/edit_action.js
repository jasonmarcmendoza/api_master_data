//Entities
const makeAction = require("../../../entities/admin/actions");

module.exports = function makeEditAction({ actionsDB }) {
  return async function editAction(Code, actionInfo, SessionId) {
    await makeAction.editAction(Code, actionInfo);
    const {
      U_ACTION_NAME,
      U_MODULE_CODE,
      U_UPDATED_BY,
      U_IS_ACTIVE
    } = actionInfo;

    const exists = await actionsDB.findActionById(Code, SessionId);
    if (!exists) {
      throw new Error("Action does not exist.");
    }

    const nameExist = await actionsDB.findActionByName(
      U_ACTION_NAME,
      U_MODULE_CODE,
      SessionId
    );
    if (nameExist[0] && nameExist[0].Code != Code) {
      throw new Error("Action name already exists");
    }
    const result = await actionsDB.editAction(
      Code,
      U_ACTION_NAME,
      U_MODULE_CODE,
      U_IS_ACTIVE,
      U_UPDATED_BY,
      SessionId
    );

    if (result.status !== 204) {
      throw new Error("Update failed.");
    }

    // const data = {
    //   message: "Successfull..",
    //   results: result.rows[0]
    // };

    return;
  };
};
