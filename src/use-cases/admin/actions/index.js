//Database
const actionsDB = require("../../../data-access/admin/actions");

const makeListActions = require("./list_actions");
const makeAddAction = require("./add_action");
const makeEditAction = require("./edit_action");

const listActions = makeListActions({ actionsDB });
const addAction = makeAddAction({ actionsDB });
const editAction = makeEditAction({ actionsDB });

module.exports = {
  listActions,
  addAction,
  editAction
};
