//Entities
const makeAction = require("../../../entities/admin/actions");

module.exports = function makeAddModule({ actionsDB }) {
  return async function addModule(actionInfo, SessionId) {
    await makeAction.addAction(actionInfo);
    const { U_ACTION_NAME, U_MODULE_CODE, U_CREATED_BY } = actionInfo;

    const exists = await actionsDB.findActionByName(
      U_ACTION_NAME,
      U_MODULE_CODE,
      SessionId
    );
    if (exists.length) {
      throw new Error("Action name already exists in selected module.");
    }

    delete actionInfo.SessionId;
    const result = await actionsDB.insertAction(actionInfo, SessionId);

    if (!result) {
      throw new Error("Insert failed.");
    }

    const data = {
      message: "Successfull..",
      results: result
    };

    return data;
  };
};
