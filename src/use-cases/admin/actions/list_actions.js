module.exports = function makeListActions({ actionsDB }) {
  return async function listActions(SessionId) {
    const link = `/b1s/v1/$crossjoin(U_ACTIONS,U_MODULES)?$expand=U_ACTIONS($select=Code,U_ACTION_NAME,U_MODULE_CODE,U_IS_ACTIVE),U_MODULES($select=Code,Name)&$filter=U_ACTIONS/U_MODULE_CODE eq U_MODULES/Code`;
    const result = await actionsDB.listActions(link, SessionId);

    if (result.length < 1) {
      return { message: "No actions found." };
    }

    const finalActions = [];
    result.forEach(action => {
      finalActions.push({
        ...action[`U_ACTIONS`],
        U_MODULE_NAME: action[`U_MODULES`].Name
      });
    });

    const data = {
      message: "Successfull..",
      actions: finalActions
    };

    return data;
  };
};
