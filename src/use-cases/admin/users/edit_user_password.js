//Entities

module.exports = function makeEditUser({ usersDB }) {
  return async function editUser(U_USERNAME, U_PASSWORD, SessionId) {
    if (!U_USERNAME) {
      throw new Error("User name must be provided.");
    }

    if (!U_PASSWORD) {
      throw new Error("Password must be provided.");
    }

    const exists = await usersDB.findUserByUserName(SessionId, U_USERNAME);
    if (exists.length < 1) {
      throw new Error("Employee does not exist.");
    }

    const result = await usersDB.editUserPassword(
      exists[0].Code,
      U_PASSWORD,
      SessionId
    );

    if (result.status !== 204) {
      throw new Error("Update failed.");
    }

    // const data = {
    //   message: "Successfull..",
    //   results: result.rows[0]
    // };

    return;
  };
};
