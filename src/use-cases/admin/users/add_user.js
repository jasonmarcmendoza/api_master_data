//Entities
const makeUser = require("../../../entities/admin/users");

module.exports = function makeAddUser({ usersDB, employeesDB, companyDB }) {
  return async function addUser(userInfo, SessionId) {
    await makeUser.addUser(userInfo);
    const { U_EMPLOYEE_CODE, U_COMPANY_CODE, Code } = userInfo;

    const employeeCodeExists = await usersDB.findUserByEmployeeCode(
      SessionId,
      U_EMPLOYEE_CODE
    );

    if (
      employeeCodeExists.length &&
      employeeCodeExists[0].U_COMPANY_CODE == U_COMPANY_CODE
    ) {
      throw new Error("Employee Code already exists.");
    }
    // const nameExists = await usersDB.findUserByName(
    //   SessionId,
    //   U_FIRST_NAME,
    //   U_MIDDLE_NAME,
    //   U_LAST_NAME,
    //   U_SUFFIX_NAME
    // );

    // if (nameExists.length) {
    //   throw new Error("Name already exists");
    // }

    userInfo.U_CREATED_BY = userInfo.Code;
    delete userInfo.SessionId;
    delete userInfo.Code;

    let result = await usersDB.insertUser(SessionId, userInfo);

    if (!result) {
      throw new Error("Insert failed.");
    }

    const companies = await companyDB.listCompanies(SessionId);
    const details = await employeesDB.getEmployeeInfoByID(
      result.U_EMPLOYEE_CODE,
      companies.find(company => company.ID == U_COMPANY_CODE).NAME
    );
    delete details["odata.metadata"];

    result = { ...result, ...details };

    const data = {
      message: "Successfull..",
      results: result
    };

    return data;
  };
};
