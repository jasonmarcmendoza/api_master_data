module.exports = function makeListUsers({ usersDB, companyDB }) {
  return async function listUsers(SessionId) {

    const companies = await companyDB.listCompanies(SessionId)

    const result = await usersDB.listUsers(SessionId, companies);



    if (result.length < 1) {
      return { message: "No users found." };
    }




    const data = {
      message: "Successfull..",
      users: result
    };

    return data;
  };
};
