//Database
const usersDB = require("../../../data-access/admin/users");
const rolesDB = require("../../../data-access/admin/roles");
const employeesDB = require("../../../data-access/sap/employees");
const companyDB = require("../../../data-access/sap/companies");

const makeListUsers = require("./list_users");
const makeAddUser = require("./add_user");
const makeEditUser = require("./edit_user");
const makeEditResetPassword = require("./reset_user_password");
const makeEditUserPassword = require("./edit_user_password");
const makeUpdateSAPUser = require("./update_sap_user");

const listUsers = makeListUsers({ usersDB, companyDB });
const addUser = makeAddUser({ usersDB, employeesDB, companyDB });
const editUser = makeEditUser({ usersDB, rolesDB });
const resetUserPasword = makeEditResetPassword({ usersDB });
const editUserPassword = makeEditUserPassword({ usersDB });
const updateSAPUser = makeUpdateSAPUser({ usersDB, companyDB });

module.exports = {
  listUsers,
  addUser,
  editUser,
  resetUserPasword,
  editUserPassword,
  updateSAPUser
};
