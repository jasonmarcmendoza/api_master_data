module.exports = function makeEditResetPassword({ usersDB }) {
  return async function editResetPassword(Code, userInfo, SessionId) {
    const { U_UPDATED_BY } = userInfo;
    if (!Code) {
      throw new Error("Employee code must be provided.");
    }
    if (isNaN(Code)) {
      throw new Error("Invalid employee code.");
    }
    if (!U_UPDATED_BY) {
      throw new Error("Updated by id must be provided.");
    }
    if (isNaN(U_UPDATED_BY)) {
      throw new Error("Invalid updated by id.");
    }
    const exists = await usersDB.findUserById(Code, SessionId);
    if (!exists) {
      throw new Error("User does not exist.");
    }

    const result = await usersDB.resetPassword(
      Code,
      exists.U_USERNAME,
      U_UPDATED_BY,
      SessionId
    );

    if (result.status !== 204) {
      throw new Error("Update failed.");
    }

    // //Decrypt user details
    // result.rows[0].first_name = encryption.decrypt(result.rows[0].first_name);
    // result.rows[0].last_name = encryption.decrypt(result.rows[0].last_name);
    // if (result.rows[0].middle_name)
    //   result.rows[0].middle_name = encryption.decrypt(
    //     result.rows[0].middle_name
    //   );
    // if (result.rows[0].suffix_name)
    //   result.rows[0].suffix_name = encryption.decrypt(
    //     result.rows[0].suffix_name
    //   );
    // if (result.rows[0].email)
    //   result.rows[0].email = encryption.decrypt(result.rows[0].email);

    // const data = {
    //   message: "Successfull..",
    //   results: result.rows[0]
    // };

    return;
  };
};
