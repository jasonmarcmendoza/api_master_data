//Entities
const makeUser = require("../../../entities/admin/users");

module.exports = function makeEditUser({ usersDB, rolesDB }) {
  return async function editUser(Code, userInfo, SessionId) {
    await makeUser.editUser(Code, userInfo);
    const { U_EMPLOYEE_CODE } = userInfo;
    let {
      U_COMPANY_CODE,
      U_MIDDLE_NAME,
      U_LAST_NAME,
      U_SUFFIX_NAME,
      email
    } = userInfo;

    const exists = await usersDB.findUserById(Code, SessionId);
    if (!exists) {
      throw new Error("Employee does not exist.");
    }

    // const role = await rolesDB.findRoleById(role_id)

    // if (role.rowCount < 1) {
    //   throw new Error('Role does not exist.')
    // }

    // if (role.rows[0].role_name.split('-')[0] === 'actng') {
    //   if (!accounting_group_id) {
    //     throw new Error('Accounting Group must be provided.')
    //   }
    // }

    const usernameExists = await usersDB.findUserByEmployeeCode(
      SessionId,
      U_EMPLOYEE_CODE
    );

    if (
      usernameExists[0] &&
      usernameExists[0].Code != Code &&
      usernameExists[0].U_COMPANY_CODE == U_COMPANY_CODE
    ) {
      throw new Error("Employee code already exists.");
    }

    // const nameExists = await usersDB.findUserByName(
    //   SessionId,
    //   U_FIRST_NAME,
    //   U_MIDDLE_NAME,
    //   U_LAST_NAME,
    //   U_SUFFIX_NAME
    // );

    // if (nameExists[0] && nameExists[0].Code !== Code) {
    //   throw new Error("Name already exists");
    // }

    delete userInfo.SessionId;

    const result = await usersDB.editUser(SessionId, userInfo, Code);

    if (result.status !== 204) {
      throw new Error("Update failed.");
    }

    // //Decrypt user details
    // result.rows[0].first_name = encryption.decrypt(result.rows[0].first_name);
    // result.rows[0].last_name = encryption.decrypt(result.rows[0].last_name);
    // if (result.rows[0].middle_name)
    //   result.rows[0].middle_name = encryption.decrypt(
    //     result.rows[0].middle_name
    //   );
    // if (result.rows[0].suffix_name)
    //   result.rows[0].suffix_name = encryption.decrypt(
    //     result.rows[0].suffix_name
    //   );
    // if (result.rows[0].email)
    //   result.rows[0].email = encryption.decrypt(result.rows[0].email);

    // const data = {
    //   message: "Successfull..",
    //   results: result.rows[0]
    // }

    return;
  };
};
