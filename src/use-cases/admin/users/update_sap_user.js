module.exports = function makeUpdateUser({ usersDB, companyDB }) {
  return async function updateUser(Code, SessionId) {
    const exists = await usersDB.findUserById(Code, SessionId);
    if (!exists) {
      throw new Error("Employee does not exist.");
    }

    const companies = await companyDB.listCompanies(SessionId);
    const user = await usersDB.findEmployeeUser(
      exists.U_EMPLOYEE_CODE,
      companies.find(company => company.ID == exists.U_COMPANY_CODE).NAME
    );

    console.log(user);

    if (user.length < 1) {
      throw new Error("Unknown error!");
    }

    if (user[0].USER_CODE != exists.U_USERNAME) {
      const userInfo = {
        U_USERNAME: user[0].USER_CODE
      };

      const result = await usersDB.editUser(SessionId, userInfo, Code);

      if (result.status !== 204) {
        throw new Error("Update failed.");
      }
    }
    return {
      message: "Successful...",
      user: {
        Code: exists.Code,
        U_USERNAME: user[0].USER_CODE
      }
    };
  };
};
