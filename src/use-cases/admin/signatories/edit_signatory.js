//Entities
const makeSignatory = require("../../../entities/admin/signatories");
module.exports = function makeEditSignatory({ signatoriesDB, moment }) {
  return async function editSignatory(Code, signatoryInfo, SessionId) {
    await makeSignatory.editSignatory(Code, signatoryInfo, SessionId);

    const result = await signatoriesDB.editSignatory(
      Code,
      signatoryInfo,
      SessionId
    );
    if (result.status != 204) {
      throw new Error("Update failed.");
    }

    return;
  };
};
