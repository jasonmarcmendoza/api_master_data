//Database
const signatoriesDB = require("../../../data-access/admin/signatories");
const companyDB = require("../../../data-access/sap/companies");

const moment = require("moment-timezone");

const makeListSignatory = require("./list_signatories");
const makeListCompanySignatories = require("./get_company_signatory");
const makeAddSignatory = require("./add_signatory");
const makeEditSignatory = require("./edit_signatory");

const listSignatories = makeListSignatory({ signatoriesDB, companyDB });
const addSignatory = makeAddSignatory({ signatoriesDB });
const editSignatory = makeEditSignatory({ signatoriesDB, moment });
const listCompanySignatories = makeListCompanySignatories({
  signatoriesDB,
  companyDB
});

module.exports = {
  listSignatories,
  addSignatory,
  editSignatory,
  listCompanySignatories
};
