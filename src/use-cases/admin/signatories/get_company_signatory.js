module.exports = function makeListCompanySignatories({
  signatoriesDB,
  companyDB
}) {
  return async function listCompanySignatories(SessionId) {
    const companies = await companyDB.listCompanies(SessionId);
    const result = await signatoriesDB.listSignatories(SessionId, companies);
    const data = {
      message: "Successfull..",
      signatories: result
    };

    return data;
  };
};
