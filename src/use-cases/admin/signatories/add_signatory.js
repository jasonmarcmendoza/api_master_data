//Entities
const makeSignatories = require("../../../entities/admin/signatories");

module.exports = function makeAddSignatory({ signatoriesDB }) {
  return async function addSignatory(signatoryInfo, SessionId) {
    await makeSignatories.addSignatory(signatoryInfo);

    const exists = await signatoriesDB.findSignatoryById(
      signatoryInfo.U_EMPLOYEE_CODE,
      signatoryInfo.U_COMPANY_CODE,
      SessionId
    );
    if (exists.length > 0) {
      throw new Error("Signatory already exists.");
    }

    const result = await signatoriesDB.insertSignatory(
      signatoryInfo,
      SessionId
    );

    const data = {
      message: "Successfull..",
      results: result
    };

    return data;
  };
};
