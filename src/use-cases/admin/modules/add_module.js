//Entities
const modules = require("../../../entities/admin/modules");

module.exports = function makeAddModule({ modulesDB }) {
  return async function addModule(moduleInfo, SessionId) {
    await modules.addModule(moduleInfo);
    const { Name, Code } = moduleInfo;

    const exists = await modulesDB.findModuleByName(Name, SessionId);
    if (exists.length) {
      throw new Error("Module name already exists.");
    }

    const result = await modulesDB.insertModule(Name, Code, SessionId);

    if (!result) {
      throw new Error("Insert failed.");
    }

    const data = {
      message: "Successfull..",
      results: result
    };

    return data;
  };
};
