//Database
const modulesDB = require("../../../data-access/admin/modules");

const makeListModules = require("./list_modules");
const makeAddModule = require("./add_module");
const makeEditModule = require("./edit_module");

const listModules = makeListModules({ modulesDB });
const addModule = makeAddModule({ modulesDB });
const editModule = makeEditModule({ modulesDB });

module.exports = {
  addModule,
  listModules,
  editModule
};
