//Entities
const modules = require("../../../entities/admin/modules");

module.exports = function makeEditModule({ modulesDB }) {
  return async function editModule(Code, moduleInfo, SessionId) {
    await modules.editModule(Code, moduleInfo);
    const { Name, U_IS_ACTIVE } = moduleInfo;

    const exists = await modulesDB.findModuleById(Code, SessionId);

    if (!exists) {
      throw new Error("Module does not exist.");
    }
    const nameExists = await modulesDB.findModuleByName(
      Name,

      SessionId
    );
    if (
      nameExists[0] &&
      nameExists[0].Code != Code &&
      nameExists[0].Name === Name
    ) {
      throw new Error("Module name already exists");
    }

    delete moduleInfo.SessionId;
    const result = await modulesDB.editModule(
      Code,
      moduleInfo,

      SessionId
    );

    if (result.status !== 204) {
      throw new Error("Update failed.");
    }

    // const data = {
    //   message: "Successfull..",
    //   results: result.rows[0]
    // };

    return;
  };
};
