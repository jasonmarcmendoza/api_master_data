module.exports = function makeListModules({ modulesDB }) {
  return async function listModules(SessionId) {
    const link = `/b1s/v1/U_MODULES?$select=Code,Name,U_IS_ACTIVE`;
    const result = await modulesDB.listModules(SessionId, link);

    if (result.length < 1) {
      return { message: "No modules found." };
    }

    const data = {
      message: "Successfull..",
      modules: result
    };

    return data;
  };
};
