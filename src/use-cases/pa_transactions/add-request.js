const addRequest = ({ transactionsDb, moment }) => {
    return async function post(info) {

      if(!info.stage_id){
        throw new Error(`Please input stage ID`)
      }

      if(!info.requested_by){
        throw new Error(`Please input requester ID`)
      }

      if(!info.company_id){
        throw new Error(`Please input company ID`)
      }

      if(info.items.length === 0){
        throw new Error(`Please input items`)
      }

      const d = new Date().toDateString();
      const date = moment(d).format("YYYY-MM-DD");
      const t = new Date().toTimeString();
      const time = await fixTime(t);

      info.date_requested = date
      info.time_requested = time

      const items = info.items

      const transactionCountDetails = await transactionsDb.selectTransactionCount({info: {cookie: info.cookie}})

      const transactionId = transactionCountDetails.data+1

   
      const transactionJson = {
        cookie: info.cookie,
        Code: transactionId,
        Name: transactionId,
        U_DATE_REQUESTED: info.date_requested,
        U_TIME_REQUESTED: info.time_requested,
        U_STATUS: "Pending",
        U_REQUESTED_BY: info.requested_by,
        U_COMPANY_CODE: info.company_id
      }

      const insertTransaction = await transactionsDb.addTransaction({info: { ...transactionJson }})

      for(let i =0 ; i<items.length; i++){

        const requestCountDetails = await transactionsDb.selectRequestCount({info: {cookie: info.cookie}})

        const requestId = requestCountDetails.data+1

        const stageDetails = await transactionsDb.selectOneStageById({info: {
          id: info.stage_id,
          cookie: info.cookie
        }})
        
        const stage = stageDetails.data.U_STAGE

        let requestJson = {
          cookie: info.cookie,
          Code: requestId,
          Name: requestId,
          U_ITEM_ID: items[i].item_id,
          U_DATE_FROM: info.date_from,
          U_TIME_FROM: info.time_from,
          U_DATE_TO: info.date_to,
          U_TIME_TO: info.time_to,
          U_STAGE_ID: info.stage_id,
          U_PRICE: items[i].price,
          U_OLD_PRICE: items[i].old_price,
          U_TRANSACTION_ID: transactionId,
          U_STATUS: `Pending`
        }
     
        const insertRequest = await transactionsDb.addRequest({info: { ...requestJson }})

        for(let i = 1; i <= stage; i++){


          const approvalCountDetails = await transactionsDb.selectApprovalCount({info: {cookie: info.cookie}})

          const approvalId = approvalCountDetails.data+1

          let approvalJson = {
            cookie: info.cookie,
            Code: approvalId,
            Name: approvalId,
            U_REQUEST_ID: requestId,
            U_LEVEL: i
          }
          
          const insertApproval = await transactionsDb.addApproval({info: { ...approvalJson }})

        }

      }

      return true
  }
  };


  const fixTime = t => {
    try {
      const arr = t.split(":", 2);
      const raw = `${arr[0]}${arr[1]}`;
      const time = raw;
      return time;
    } catch (e) {
      console.log("Error: ", e);
    }
  };
  
  module.exports = addRequest;
  