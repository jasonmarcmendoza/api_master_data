const disapproveRequest = ({ transactionsDb, usersDb, moment }) => {
    return async function put(info) {

        if(!info.employee_id){
            throw new Error(`Please input employee ID`)
        }

        const d = new Date().toDateString();
        const date = moment(d).format("YYYY-MM-DD");
        const t = new Date().toTimeString();
        const time = await fixTime(t);

        const userDetails = await usersDb.selectOneUser({info: {
            id: info.employee_id,
            cookie: info.cookie
        }})

              
        const approverStage = userDetails.data[0].U_PA_STAGES.U_STAGE

        const approvalsDetails = await transactionsDb.selectApprovalsByRequest({info: {
            id: info.id,
            cookie: info.cookie
        }})

        const approvals = approvalsDetails.data

        let approvalIdToBeDisapproved, approvalStage
        
        for(let i =0 ; i<approvals.length; i++){
            if(!approvals[i].U_APPROVAL_BY){
                approvalIdToBeDisapproved = approvals[i].Code
                approvalStage = approvals[i].U_LEVEL
                break;
            }

        }

        if(approverStage != approvalStage){
            throw new Error(`Not your level`)
        }
      

        const requestDetails = await transactionsDb.selectOneRequestByIdSimple({info: {
            id: approvalsDetails.data[0].U_REQUEST_ID
        }})

        const transactionId = requestDetails[0].U_TRANSACTION_ID
        

        const updateApproval = await transactionsDb.updateApprovals({info: {
            id: approvalIdToBeDisapproved,
            cookie: info.cookie,
            U_APPROVAL_BY: info.employee_id,
            U_IS_APPROVED: 0,
            U_DATE_OF_APPROVAL: date,
            U_TIME_OF_APPROVAL: time
        }})
        
        const updateRequest = await transactionsDb.updateRequests({info: {
            cookie: info.cookie,
            id: info.id,
            U_STATUS: 'Disapproved'
        }})



        const requestPartners = await transactionsDb.selectOneTransactionWithRequests({info: {
            id: transactionId
        }})
     

        let flag = true
        for(let i = 0 ; i <requestPartners.length; i++){
            if(requestPartners[i].REQUEST_STATUS === 'Pending'){
                flag = false
            }
        }

        if(flag){
            const updateTransaction = await transactionsDb.updateTransaction({info: {
                cookie: info.cookie,
                id: transactionId,
                U_STATUS: 'Processed'
            }})
        }


        return true
    
  }
  };
  
  const fixTime = t => {
    try {
      const arr = t.split(":", 2);
      const raw = `${arr[0]}${arr[1]}`;
      const time = raw;
      return time;
    } catch (e) {
      console.log("Error: ", e);
    }
  };

  module.exports = disapproveRequest;
  