const cancelTransaction = ({ transactionsDb }) => {
    return async function put(info) {

        const transactionDetails = await transactionsDb.selectOneTransactionSimple({info: {id: info.id}})

        info.company_id = transactionDetails[0].U_COMPANY_CODE
       
    
          const companyDetails = await transactionsDb.selectOneCompany({info: {id: info.company_id}})
    
          info.company = companyDetails[0].NAME

        
        const requests = await transactionsDb.selectOneTransaction({info})
      

        const updateTransaction = await transactionsDb.updateTransaction({info: {
            cookie: info.cookie,
            id: info.id,
            U_STATUS: 'Cancelled'
        }})

        for(let i = 0; i<requests.length; i++){

            const updateRequests = await transactionsDb.updateRequests({info: {
                cookie: info.cookie,
                id: requests[i].REQUEST_ID,
                U_STATUS: 'Cancelled'
            }})

        }

        return company_id
    
    
  
  }
  };
  
  module.exports = cancelTransaction;
  