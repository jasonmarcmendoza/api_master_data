const selectAllTransactions = ({ transactionsDb, usersDb }) => {
    return async function get(info) {
      
      if(!info.date_from){
        throw new Error(`Please input date from`)
      }

      if(!info.date_to){
        throw new Error(`Please input date to`)
      }

    const transactions = await transactionsDb.selectAllTransactions({info})

    for(let i = 0; i < transactions.length; i++){

      const companyDetails = await transactionsDb.selectOneCompany({info: {id: transactions[i].REQUESTER_COMPANY}})
    
      const company = companyDetails[0].NAME

      const employeeDetails = await usersDb.selectOneUserByCompany({info: {
        company: company,
        id: transactions[i].EMP_ID
      }})
      
      transactions[i].REQUESTER = employeeDetails[0].EMPLOYEE_NAME
    }

    return transactions
  }
  };
  
  module.exports = selectAllTransactions;
  