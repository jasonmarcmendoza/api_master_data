const selectOneTransaction = ({ transactionsDb }) => {
    return async function get(info) {
     
      const transactionDetails = await transactionsDb.selectOneTransactionSimple({info: {id: info.id}})

      info.company_id = transactionDetails[0].U_COMPANY_CODE

      
      const companyDetails = await transactionsDb.selectOneCompany({info: {id: info.company_id}})


      info.company = companyDetails[0].NAME
    
    const transaction = await transactionsDb.selectOneTransaction({info: {...info}})

    return transaction
  }
  };
  
  module.exports = selectOneTransaction;
  