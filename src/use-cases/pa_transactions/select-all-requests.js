const selectAllRequests = ({ transactionsDb, itemsDb, usersDb }) => {
    return async function get(info) {

        if(!info.employee_id){
            throw new Error(`Please input employee ID`)
        }

        if(!info.stage_id){
            throw new Error(`Please input stage ID`)
        }

        if(!info.date_from){
            throw new Error(`Please input date from`)
        }

        if(!info.date_to){
            throw new Error(`Please input date to`)
        }

        const stageDetails = await transactionsDb.selectOneStageById({info:{
            id: info.stage_id,
            cookie: info.cookie
        }})

        const stage = stageDetails.data.U_STAGE

        const requests = await transactionsDb.selectAllRequests({info})

        for(let i = 0 ; i <requests.length; i++){

            const companyDetails = await transactionsDb.selectOneCompany({info: {id: requests[i].REQUESTER_COMPANY}})
    
            const company = companyDetails[0].NAME
    

            const itemDetails = await itemsDb.selectOneItemById({info: {
                id: requests[i].ITEM_ID,
                company: requests[i].DB_NAME
            }})

            requests[i].ITEM_NAME = itemDetails[0].ITEM_NAME

            const employeeDetails = await usersDb.selectOneUserByCompany({info: {
                company: company,
                id: requests[i].EMP_ID
              }})
            requests[i].REQUESTER_NAME = employeeDetails[0].EMPLOYEE_NAME

            const approvalsDetails = await transactionsDb.selectApprovalsByRequest({info: {
                id: requests[i].REQUEST_ID,
                cookie: info.cookie
            }})

            
            requests[i].WILL_APPROVE_DISAPPROVE = true

            // //higher stage. will not approve
            // if(stage > requests[i].STAGE){
            //     requests[i].WILL_APPROVE_DISAPPROVE = false
            // }

           
            const a = approvalsDetails.data
           
           
            // //done approve
            // for(let i2 = 0 ; i2 <a.length ; i2++){
            //     if(info.employee_id === a[i2].U_APPROVAL_BY){
            //         requests[i].WILL_APPROVE_DISAPPROVE =  false         
            //     }
            // }

            // get current level
            for(let i2 = 0 ; i2 <a.length ; i2++){  
                if(!a[i2].U_APPROVAL_BY){
                    requests[i].CURRENT_STAGE = await a[i2].U_LEVEL  
                    break      
                }
            }

            //if approver is not equal to current stage of approval
            if(stage != requests[i].CURRENT_STAGE){
                requests[i].WILL_APPROVE_DISAPPROVE = false
            }


        }

        
       
        return requests  
  
  }
  };
  
  module.exports = selectAllRequests;
  