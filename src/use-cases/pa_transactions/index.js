const { transactionsDb } = require("../../data-access/pa_transactions/index")
const { usersDb } = require("../../data-access/pa_users/index")
const { itemsDb } = require("../../data-access/pa_items/index")

const moment = require("moment")

const addRequest = require("./add-request")
const selectAllTransactions = require("./select-all-transactions")
const selectOneTransaction  = require("./select-one-transaction")
const selectAllRequests = require("./select-all-requests")
const cancelTransaction = require("./cancel-transaction")
const disapproveRequest = require("./disapprove-request")
const approveRequest = require("./approve-request")
const selectAllLevels = require("./select-all-levels")
const selectApprovalDetails = require("./select-approval-details")

const u_addRequest = addRequest({ transactionsDb, moment })
const u_selectAllTransactions = selectAllTransactions({ transactionsDb, usersDb })
const u_selectOneTransaction = selectOneTransaction({transactionsDb})
const u_selectAllRequests = selectAllRequests ({transactionsDb, itemsDb, usersDb})
const u_cancelTransaction = cancelTransaction({transactionsDb})
const u_disapproveRequest = disapproveRequest({transactionsDb, usersDb, moment})
const u_approveRequest = approveRequest({transactionsDb, usersDb, moment})
const u_selectAllLevels = selectAllLevels({ transactionsDb })
const u_selectApprovalDetails = selectApprovalDetails({ transactionsDb, usersDb })

const services = Object.freeze({
    u_addRequest,
    u_selectAllTransactions,
    u_selectOneTransaction,
    u_selectAllRequests,
    u_cancelTransaction,
    u_disapproveRequest,
    u_approveRequest,
    u_selectAllLevels,
    u_selectApprovalDetails
  })
  
  module.exports = services
  module.exports = {
    u_addRequest,
    u_selectAllTransactions,
    u_selectOneTransaction,
    u_selectAllRequests,
    u_cancelTransaction,
    u_disapproveRequest,
    u_approveRequest,
    u_selectAllLevels,
    u_selectApprovalDetails
  };
  