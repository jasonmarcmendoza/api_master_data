const approveRequest = ({ transactionsDb, usersDb, moment }) => {
    return async function put(info) {


        if(!info.company){
            throw new Error(`Please input company`)
        }

        if(!info.employee_id){
            throw new Error(`Please input employee ID`)
        }
        
        const d = new Date().toDateString();
        const date = moment(d).format("YYYY-MM-DD");
        const t = new Date().toTimeString();
        const time = await fixTime(t);

        const userDetails = await usersDb.selectOneUser({info: {
            id: info.employee_id,
            cookie: info.cookie
        }})

              
        const approverStage = userDetails.data[0].U_PA_STAGES.U_STAGE

        const approvalsDetails = await transactionsDb.selectApprovalsByRequest({info: {
            id: info.id,
            cookie: info.cookie
        }})
    
        const approvals = approvalsDetails.data

        let approvalIdToBeApproved, approvalStage

        let approvalStages = approvals[approvals.length-1].U_LEVEL

        let requestId = approvals[approvals.length-1].U_REQUEST_ID

        const requestDetails = await transactionsDb.selectOneRequestById({info: {id: requestId, company: info.company}})
 
        const transactionId = requestDetails[0].U_TRANSACTION_ID
        
        const itemId = requestDetails[0].U_ITEM_ID

        const oldPrice = requestDetails[0].CURRENT_PRICE

        const newPrice = requestDetails[0].U_PRICE

     
        for(let i =0 ; i<approvals.length; i++){
            if(!approvals[i].U_APPROVAL_BY){
                approvalIdToBeApproved = approvals[i].Code
                approvalStage = approvals[i].U_LEVEL
                break;
            }

        }

     
        
        if(approverStage != approvalStage){
            throw new Error(`Not your level`)
        }

        if(approvalStages == approverStage){
            // update approval
         
            const updateApproval = await transactionsDb.updateApprovals({info: {
                id: approvalIdToBeApproved,
                cookie: info.cookie,
                U_APPROVAL_BY: info.employee_id,
                U_IS_APPROVED: 1,
                U_DATE_OF_APPROVAL: date,
                U_TIME_OF_APPROVAL: time
            }})

            //update request status
            const updateRequest = await transactionsDb.updateRequests({info: {
                cookie: info.cookie,
                id: requestId,
                U_STATUS: 'Approved'
            }})
            //update to sap
            const updateSap = await transactionsDb.sapUpdateItemPrice({info: {
                company: info.company,
                newPrice: newPrice,
                id: itemId
            }})

            //post to logs
            const priceLogsDetails = await transactionsDb.selectPriceLogCount({info: {
                cookie: info.cookie,
            }})

          
            const priceLogId = priceLogsDetails.data+1

            const logJson = {
                cookie: info.cookie,
                Code: priceLogId,
                Name: priceLogId,
                U_REQUEST_ID: requestId,
                U_ITEM_ID: itemId,
                U_CREATED_DATE: date,
                U_CREATED_TIME: time,
                U_OLD_PRICE: oldPrice,
                U_NEW_PRICE: newPrice
            }

            const insertPriceLog = await transactionsDb.addPriceLog({info: {...logJson}})     
            
            const requestPartners = await transactionsDb.selectOneTransactionWithRequests({info: {
                id: transactionId
            }})
         

            let flag = true
            for(let i = 0 ; i <requestPartners.length; i++){
                if(requestPartners[i].REQUEST_STATUS === 'Pending'){
                    flag = false
                }
            }

        

            if(flag){
                const updateTransaction = await transactionsDb.updateTransaction({info: {
                    cookie: info.cookie,
                    id: transactionId,
                    U_STATUS: 'Processed'
                }})
            }
           
        }else {
            const updateApproval = await transactionsDb.updateApprovals({info: {
                id: approvalIdToBeApproved,
                cookie: info.cookie,
                U_APPROVAL_BY: info.employee_id,
                U_IS_APPROVED: 1,
                U_DATE_OF_APPROVAL: date,
                U_TIME_OF_APPROVAL: time
            }})
        }

        

        return true
    
  }
  };

  const fixTime = t => {
    try {
      const arr = t.split(":", 2);
      const raw = `${arr[0]}${arr[1]}`;
      const time = raw;
      return time;
    } catch (e) {
      console.log("Error: ", e);
    }
  };
  
  module.exports = approveRequest;
  