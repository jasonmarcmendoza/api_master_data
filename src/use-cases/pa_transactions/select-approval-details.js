const selectApprovalDetails = ({ transactionsDb, usersDb }) => {
    return async function get(info) {


        if(!info.company){
            throw new Error(`Please input company`)
        }

        const approvalsDetails = await transactionsDb.selectApproversForReports({info: {
            id: info.id,
            company: info.company
        }})
     
        for(let i = 0 ; i< approvalsDetails.length; i++){
           
            if(approvalsDetails[i].U_IS_APPROVED !== null){
            const companyDetails = await transactionsDb.selectOneCompany({info: {id: approvalsDetails[i].APPROVER_COMPANY}})
    
            const company = companyDetails[0].NAME

            const employeeDetails = await usersDb.selectOneUserByCompany({info: {
                company: company,
                id: approvalsDetails[i].U_EMPLOYEE_CODE
              }})
              approvalsDetails[i].APPROVER_NAME = employeeDetails[0].EMPLOYEE_NAME
            }else{
            approvalsDetails[i].APPROVER_NAME = null
            }
        }
        
        return approvalsDetails
  }
  };
  
  module.exports = selectApprovalDetails;
  