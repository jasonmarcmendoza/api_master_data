const selectAllLevels = ({ transactionsDb }) => {
    return async function get(info) {

       
     
        const levelDetails = await transactionsDb.selectAllLevels({info: {
            cookie: info.cookie
        }})

       
        const levels = levelDetails.data.value

        return levels  
  
  }
  };
  
  module.exports = selectAllLevels;
  