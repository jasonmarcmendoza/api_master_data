const selectAllReports = ({ transactionsDb, itemsDb, usersDb }) => {
    return async function get(info) {

    if(!info.date_from){
        throw new Error(`Please input date from`)
    }

    if(!info.date_to){
        throw new Error(`Please input date to`)
    }

      const reports = await transactionsDb.selectAllReports({info})
     

      for(let i = 0 ; i <reports.length; i++){
        

        const itemDetails = await itemsDb.selectOneItemById({info: {
          id: reports[i].ITEM_ID,
          company: reports[i].DB_NAME
        }})
       
        reports[i].ITEM_NAME = itemDetails[0].ITEM_NAME



        if(reports[i].STATUS === 'Disapproved') {
          reports[i].OLD_PRICE = reports[i].OLD_PRICE_UPON_REQUEST
          reports[i].NEW_PRICE = reports[i].REQUESTED_PRICE
         
        }

        
        const companyDetails = await transactionsDb.selectOneCompany({info: {id: reports[i].REQUESTER_COMPANY}})
    
        const company = companyDetails[0].NAME

        const employeeDetails = await usersDb.selectOneUserByCompany({info: {
          company: company,
          id: reports[i].EMP_ID
        }})
        reports[i].REQUESTER_NAME = employeeDetails[0].EMPLOYEE_NAME

      }

      return reports  

  }
  };
  
  module.exports = selectAllReports;
  