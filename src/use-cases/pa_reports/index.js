const { transactionsDb } = require("../../data-access/pa_transactions/index")
const { itemsDb } = require("../../data-access/pa_items/index")
const { usersDb } = require("../../data-access/pa_users/index")

const selectAllReports = require('./select-all-reports')

const u_selectAllReports = selectAllReports({ transactionsDb, itemsDb, usersDb })


const services = Object.freeze({
    u_selectAllReports
  })
  
  module.exports = services
  module.exports = {
    u_selectAllReports
  };
  