//Database
const statesDB = require("../../../data-access/sap/states");
const companiesDB = require("../../../data-access/sap/companies");

const makeListStates = require("./list_states");

const listStates = makeListStates({ companiesDB, statesDB });

module.exports = {
  listStates
};
