module.exports = function makeListStates({ statesDB, companiesDB }) {
  return async function listStates(SessionId, CompanyDB, Country) {
    if (!CompanyDB) {
      throw new Error("Company must be provided.");
    }
    if (!Country) {
      throw new Error("Country must be provided.");
    }
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await statesDB.listStates(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME,
      Country
    );

    if (result.length < 1) {
      return { message: "No states found." };
    }
    const data = {
      message: "Successfull..",
      states: result
    };
    return data;
  };
};
