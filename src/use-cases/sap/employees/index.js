//Database
const employeesDB = require("../../../data-access/sap/employees");
const companyDB = require("../../../data-access/sap/companies");

const makeGetEmployeeInfo = require("./get_employee_info");

const getEmployeeInfo = makeGetEmployeeInfo({ employeesDB, companyDB });

module.exports = {
  getEmployeeInfo
};
