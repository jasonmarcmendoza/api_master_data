module.exports = function makeGetEmployeeInfo({ employeesDB, companyDB }) {
  return async function getEmployeeInfo(SessionId, CompanyDB) {
    const companies = await companyDB.listCompanies(SessionId);
    CompanyDB = companies.find(company => company.ID == CompanyDB).NAME;
    const result = await employeesDB.getEmployeeInfo(SessionId, CompanyDB);

    if (result.length < 1) {
      return { message: "No employees found." };
    }
    let employees = [];
    for await (user of result) {
      if (user["EmployeesInfo"].ApplicationUserID) {
        const username = await employeesDB.getEmployeeUsername(
          user["EmployeesInfo"].ApplicationUserID,
          CompanyDB
        );
        user["EmployeesInfo"].UserCode = username.UserCode;
      }

      employees.push({
        ...user["EmployeesInfo"],
        Position: user["EmployeePosition"].Name
      });
    }

    const data = {
      message: "Successfull..",
      employees
    };

    return data;
  };
};
