module.exports = function makeListCustomers({ customersDB, companiesDB }) {
  return async function listCustomers(CompanyDB, SessionId) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await customersDB.listCustomers(
      companies.find(company => company.ID == CompanyDB).NAME
    );

    if (result.length < 1) {
      return { message: "No customers found." };
    }

    const data = {
      message: "Successful..",
      customers: result
    };
    return data;
  };
};
