//Database
const customersDB = require("../../../data-access/sap/customers");
const companiesDB = require("../../../data-access/sap/companies");

const makeListCustomers = require("./list_customers");

const listCustomers = makeListCustomers({ customersDB, companiesDB });

module.exports = {
  listCustomers
};
