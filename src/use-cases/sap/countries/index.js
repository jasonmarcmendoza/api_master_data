//Database
const countriesDB = require("../../../data-access/sap/countries");
const companiesDB = require("../../../data-access/sap/companies");

const makeListCountries = require("./list_countries");

const listCountries = makeListCountries({ companiesDB, countriesDB });

module.exports = {
  listCountries
};
