module.exports = function makeListCountries({ countriesDB, companiesDB }) {
  return async function listCountries(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await countriesDB.listCountries(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );

    if (result.length < 1) {
      return { message: "No countries found." };
    }
    const data = {
      message: "Successfull..",
      countries: result
    };
    return data;
  };
};
