//Database
const currenciesDB = require("../../../data-access/sap/currencies");
const companiesDB = require("../../../data-access/sap/companies");

const makeListCurrencies = require("./list_currencies");

const listCurrencies = makeListCurrencies({ companiesDB, currenciesDB });

module.exports = {
  listCurrencies
};
