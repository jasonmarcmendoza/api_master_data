module.exports = function makeListCurrecies({ currenciesDB, companiesDB }) {
  return async function listBPGroups(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await currenciesDB.listCurrencies(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );
    if (result.length < 1) {
      return { message: "No currencies found." };
    }
    const data = {
      message: "Successfull..",
      currencies: result
    };
    return data;
  };
};
