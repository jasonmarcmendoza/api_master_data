module.exports = function makeListSalesEmployees({
  salesEmployeesDB,
  companiesDB
}) {
  return async function listSalesEmployees(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await salesEmployeesDB.listSalesEmployees(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );
    if (result.length < 1) {
      return { message: "No sale employees found." };
    }
    const data = {
      message: "Successfull..",
      sales_employees: result
    };
    return data;
  };
};
