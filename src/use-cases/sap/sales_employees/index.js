//Database
const salesEmployeesDB = require("../../../data-access/sap/sales_employees");
const companiesDB = require("../../../data-access/sap/companies");

const makeListSalesEmployees = require("./list_sales_employees");

const listSalesEmployees = makeListSalesEmployees({
  companiesDB,
  salesEmployeesDB
});

module.exports = {
  listSalesEmployees
};
