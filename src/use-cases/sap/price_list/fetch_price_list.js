module.exports = function makeFetchPriceList({ priceListDB, companiesDB }) {
  return async function fetchPriceList(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await priceListDB.fetchPriceList(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );
    if (result.length < 1) {
      return { message: "No price list found." };
    }
    const data = {
      message: "Successfull..",
      price_list: result
    };
    return data;
  };
};
