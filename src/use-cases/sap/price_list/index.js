//Database
const priceListDB = require("../../../data-access/sap/price_list");
const companiesDB = require("../../../data-access/sap/companies");

const makeFetchPriceList = require("./fetch_price_list");

const fetchPriceList = makeFetchPriceList({
  companiesDB,
  priceListDB
});

module.exports = {
  fetchPriceList
};
