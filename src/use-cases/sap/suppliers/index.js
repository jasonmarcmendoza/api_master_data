//Database
const suppliersDB = require("../../../data-access/sap/suppliers");
const companiesDB = require("../../../data-access/sap/companies");

const makeListSupplier = require("./list_suppliers");

const listSuppliers = makeListSupplier({ suppliersDB, companiesDB });

module.exports = {
  listSuppliers
};
