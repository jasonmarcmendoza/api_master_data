module.exports = function makeListSuppliers({ suppliersDB, companiesDB }) {
  return async function listSuppliers(CompanyDB, SessionId) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await suppliersDB.listSuppliers(
      companies.find(company => company.ID == CompanyDB).NAME
    );

    if (result.length < 1) {
      return { message: "No suppliers found." };
    }

    const data = {
      message: "Successful..",
      suppliers: result
    };
    return data;
  };
};
