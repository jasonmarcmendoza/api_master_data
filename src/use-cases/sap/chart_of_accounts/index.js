//Database
const chartOfAccountsDB = require("../../../data-access/sap/chart_of_accounts");
const companiesDB = require("../../../data-access/sap/companies");

const makeListChartOfAccounts = require("./list_chart_of_accounts");

const listChartOfAccounts = makeListChartOfAccounts({
  companiesDB,
  chartOfAccountsDB
});

module.exports = {
  listChartOfAccounts
};
