module.exports = function makeListCountries({
  companiesDB,
  chartOfAccountsDB
}) {
  return async function listCountries(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await chartOfAccountsDB.listChartOfAccounts(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );

    if (result.length < 1) {
      return { message: "No chart of accounts found." };
    }
    const data = {
      message: "Successfull..",
      chart_of_accounts: result
    };
    return data;
  };
};
