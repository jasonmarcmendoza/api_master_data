module.exports = function makeListBPGroups({ BPGroupsDB, companiesDB }) {
  return async function listBPGroups(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await BPGroupsDB.listBPGroups(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );
    if (result.length < 1) {
      return { message: "No BP Groups found." };
    }
    const data = {
      message: "Successfull..",
      bp_groups: result
    };
    return data;
  };
};
