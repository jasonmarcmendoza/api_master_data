//Database
const BPGroupsDB = require("../../../data-access/sap/bp_groups");
const companiesDB = require("../../../data-access/sap/companies");

const makeListBPGroups = require("./list_bp_groups");

const listBPGroups = makeListBPGroups({ companiesDB, BPGroupsDB });

module.exports = {
  listBPGroups
};
