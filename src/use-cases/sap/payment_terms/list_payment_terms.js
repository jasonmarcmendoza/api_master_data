module.exports = function makeListPaymentTerms({
  paymentTermsDB,
  companiesDB
}) {
  return async function listPaymentTerms(SessionId, CompanyDB) {
    const companies = await companiesDB.listCompanies(SessionId);

    const result = await paymentTermsDB.listPaymentTerms(
      SessionId,
      companies.find(company => company.ID == CompanyDB).NAME
    );
    if (result.length < 1) {
      return { message: "No payment terms found." };
    }
    const data = {
      message: "Successfull..",
      payment_terms: result
    };
    return data;
  };
};
