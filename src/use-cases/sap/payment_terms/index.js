//Database
const paymentTermsDB = require("../../../data-access/sap/payment_terms");
const companiesDB = require("../../../data-access/sap/companies");

const makeListPaymentTerms = require("./list_payment_terms");

const listPaymentTerms = makeListPaymentTerms({
  companiesDB,
  paymentTermsDB
});

module.exports = {
  listPaymentTerms
};
