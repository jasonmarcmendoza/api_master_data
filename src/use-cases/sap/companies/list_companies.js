module.exports = function makeListCompanies({ companiesDB }) {
  return async function listCompanies(SessionId) {
    const result = await companiesDB.listCompanies(SessionId);
    if (result.length < 1) {
      return { message: "No companies found." };
    }
    const data = {
      message: "Successfull..",
      companies: result
    };
    return data;
  };
};
