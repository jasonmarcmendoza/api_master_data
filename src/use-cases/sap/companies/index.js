//Database
const companiesDB = require("../../../data-access/sap/companies");

const makeListCompanies = require("./list_companies");

const listCompanies = makeListCompanies({ companiesDB });

module.exports = {
  listCompanies
};
