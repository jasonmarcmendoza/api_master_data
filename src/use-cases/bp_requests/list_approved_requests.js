module.exports = function makeListApprovedRequests({ requestsDB }) {
  return async function listApprovedRequests(date_range, SessionId) {
    let query = false;

    if (
      (date_range.date_from && !date_range.date_to) ||
      (date_range.date_to && !date_range.date_from)
    ) {
      throw new Error("Invalid date range.");
    } else if (date_range.date_from && date_range.date_to) {
      query = true;
    }

    const result = await requestsDB.listApprovedRequests(
      query ? date_range : null,
      SessionId
    );

    if (result.length < 1) {
      return { message: "No approved requests found." };
    }

    const data = {
      message: "Successfull..",
      approved: result
    };

    return data;
  };
};
