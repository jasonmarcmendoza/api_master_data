module.exports = function makeListPendingRequests({ requestsDB }) {
  return async function listPendingRequests(date_range, SessionId) {
    let query = false;

    if (
      (date_range.date_from && !date_range.date_to) ||
      (date_range.date_to && !date_range.date_from)
    ) {
      throw new Error("Invalid date range.");
    } else if (date_range.date_from && date_range.date_to) {
      query = true;
    }

    const result = await requestsDB.listPendingRequests(
      query ? date_range : null,
      SessionId
    );

    if (result.length < 1) {
      return { message: "No pending requests found." };
    }

    const data = {
      message: "Successfull..",
      pending: result
    };

    return data;
  };
};
