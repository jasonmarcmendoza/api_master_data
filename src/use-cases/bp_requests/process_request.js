//Entities
const requests = require("../../entities/bp_requests/");

module.exports = function makeProcessRequest({
  requestsDB,
  companiesDB,
  BPSeriesDB
}) {
  return async function processRequest(DocEntry, requestInfo, SessionId) {
    await requests.editBPRequest(DocEntry, requestInfo);
    const companies = await companiesDB.listCompanies(SessionId);
    const companyName = companies.find(
      company => company.ID == requestInfo.U_COMPANY_CODE
    ).NAME;
    const BPSeries = await BPSeriesDB.listBPSeries(
      SessionId,
      companyName,
      requestInfo.RequestDetails[0].U_CARD_TYPE
    );

    if (!BPSeries[1]) {
      throw new Error("Series not found.");
    }

    const result = await requestsDB.processRequest(
      DocEntry,
      requestInfo,
      SessionId,
      companyName,
      BPSeries[1]
    );

    if (result.status != 204) {
      throw new Error("Process failed.");
    }

    return;
  };
};
