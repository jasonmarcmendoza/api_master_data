//Entities
const requests = require("../../entities/bp_requests/");

module.exports = function makeApproveRequest({ requestsDB }) {
  return async function approveRequest(DocEntry, requestInfo, SessionId) {
    await requests.editBPRequest(DocEntry, requestInfo);

    const result = await requestsDB.approveRequest(
      DocEntry,
      requestInfo,
      SessionId
    );

    if (result.status != 204) {
      throw new Error("Approve failed.");
    }

    return;
  };
};
