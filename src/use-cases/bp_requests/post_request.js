//Entities
const requests = require("../../entities/bp_requests/");

module.exports = function makePostRequest({ requestsDB }) {
  return async function postRequest(DocEntry, requestInfo, SessionId) {
    await requests.editBPRequest(DocEntry, requestInfo);

    const result = await requestsDB.postRequest(
      DocEntry,
      requestInfo,
      SessionId
    );

    if (result.status != 204) {
      throw new Error("Post failed.");
    }

    return;
  };
};
