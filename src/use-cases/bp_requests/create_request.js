//Entities
const requests = require("../../entities/bp_requests/");

module.exports = function makeCreateRequest({ requestsDB }) {
  return async function createRequest(requestInfo, SessionId) {
    await requests.addBPRequest(requestInfo);

    const result = await requestsDB.createRequest(requestInfo, SessionId);

    if (!result) {
      throw new Error("Insert failed.");
    }

    const data = {
      message: "Successfull..",
      result
    };

    return data;
  };
};
