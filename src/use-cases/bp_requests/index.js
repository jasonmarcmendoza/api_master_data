//Database
const requestsDB = require("../../data-access/bp_requests");
const companiesDB = require("../../data-access/sap/companies");
const BPSeriesDB = require("../../data-access/sap/bp_series");

const makeListPendingRequests = require("./list_pending_requests");
const makeListApprovedRequests = require("./list_approved_requests");
const makeListProcessedRequests = require("./list_processed_requests");
const makeCreateRequest = require("./create_request");
const makePostRequest = require("./post_request");
const makeApproveRequest = require("./approve_request");
const makeProcessRequest = require("./process_request");

const listPendingRequests = makeListPendingRequests({ requestsDB });
const listApprovedRequests = makeListApprovedRequests({ requestsDB });
const listProcessedRequests = makeListProcessedRequests({ requestsDB });
const createRequest = makeCreateRequest({ requestsDB });
const postRequest = makePostRequest({ requestsDB });
const approveRequest = makeApproveRequest({ requestsDB });
const processRequest = makeProcessRequest({
  requestsDB,
  companiesDB,
  BPSeriesDB
});

module.exports = {
  listPendingRequests,
  listApprovedRequests,
  listProcessedRequests,
  createRequest,
  postRequest,
  approveRequest,
  processRequest
};
