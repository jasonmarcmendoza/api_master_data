module.exports = function makeListProcessedRequests({ requestsDB }) {
  return async function listProcessedRequests(date_range, SessionId) {
    let query = false;

    if (
      (date_range.date_from && !date_range.date_to) ||
      (date_range.date_to && !date_range.date_from)
    ) {
      throw new Error("Invalid date range.");
    } else if (date_range.date_from && date_range.date_to) {
      query = true;
    }

    const result = await requestsDB.listProcessedRequests(
      query ? date_range : null,
      SessionId
    );

    if (result.length < 1) {
      return { message: "No processed requests found." };
    }

    const data = {
      message: "Successfull..",
      processed: result
    };

    return data;
  };
};
