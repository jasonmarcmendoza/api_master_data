const express = require("express");
const router = express.Router();

const {
  getListApprovedRequests,
  getListPendingRequests,
  getListProcessedRequests,
  postCreateRequest,
  putPostRequest,
  putApproveRequest,
  putProcessRequest
} = require("../../controllers/bp_requests");

const makeCallback = require("../../express-callback");

router.post("/pending", makeCallback(getListPendingRequests));
router.post("/approved", makeCallback(getListApprovedRequests));
router.post("/processed", makeCallback(getListProcessedRequests));
router.post("/create", makeCallback(postCreateRequest));
router.put("/post/:DocEntry", makeCallback(putPostRequest));
router.put("/approve/:DocEntry", makeCallback(putApproveRequest));
router.put("/process/:DocEntry", makeCallback(putProcessRequest));

module.exports = router;
