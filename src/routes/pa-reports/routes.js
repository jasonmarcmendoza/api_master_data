
  const {
    c_selectAllReports
  } = require("../../controllers/pa_reports/index");
  
  const routes = ({ router, makeExpressCallback  }) => {
   
    router.post("/select", makeExpressCallback(c_selectAllReports));
   
  
    return router;
  };
 
  module.exports = routes;
  