const express = require("express");
const router = express.Router();

const {
  getListActions,
  postAddAction,
  putEditAction
} = require("../../../controllers/admin/actions");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListActions));
router.post("/add_action", makeCallback(postAddAction));
router.put(
  "/edit_action/:Code",

  makeCallback(putEditAction)
);

module.exports = router;
