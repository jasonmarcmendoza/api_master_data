const express = require("express");
const router = express.Router();

const {
  getListModules,
  postAddModule,
  putEditModule
} = require("../../../controllers/admin/modules");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListModules));
router.post("/add_module", makeCallback(postAddModule));
router.put("/edit_module/:Code", makeCallback(putEditModule));

module.exports = router;
