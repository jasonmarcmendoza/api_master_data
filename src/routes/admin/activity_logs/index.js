const express = require("express");
const router = express.Router();

const {
  getListActivityLogs
} = require("../../../controllers/admin/activity_logs");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListActivityLogs));

module.exports = router;
