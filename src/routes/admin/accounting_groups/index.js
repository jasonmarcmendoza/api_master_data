const express = require("express");
const router = express.Router();

const {
  getListAccountingGroups,
  postAccountingGroup,
  putAccountingGroup
} = require("../../../controllers/admin/accounting_groups");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListAccountingGroups));
router.post(
  "/add_accounting_group",
  authorization,
  makeCallback(postAccountingGroup)
);
router.put(
  "/edit_accounting_group/:accounting_group_id",
  authorization,
  makeCallback(putAccountingGroup)
);

module.exports = router;
