const express = require("express");
const router = express.Router();

const {
  getListUsers,
  postAddUser,
  putEditUser,
  putResetPassword,
  putEditUserPassword,
  putUpdateSAPUser
} = require("../../../controllers/admin/users");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListUsers));
router.post("/add_user", makeCallback(postAddUser));
router.put("/edit_user/:Code", makeCallback(putEditUser));
router.put("/reset_password/:Code", makeCallback(putResetPassword));
router.put("/edit_password", makeCallback(putEditUserPassword));
router.put("/update_sap_user/:Code", makeCallback(putUpdateSAPUser));

module.exports = router;
