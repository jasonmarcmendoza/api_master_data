const express = require("express");
const router = express.Router();

const {
  getListSignatories,
  postAddSignatory,
  putEditSignatory,
  getListCompanySignatories
} = require("../../../controllers/admin/signatories");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListSignatories));
router.get("/", makeCallback(getListCompanySignatories));
router.post("/add_signatory", makeCallback(postAddSignatory));
router.put("/edit_signatory/:Code", makeCallback(putEditSignatory));

module.exports = router;
