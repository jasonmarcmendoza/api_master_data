const express = require("express");
const router = express.Router();

const {
  getListRoles,
  postAddRole,
  putEditRole
} = require("../../../controllers/admin/roles");

const makeCallback = require("../../../express-callback");

router.post("/", makeCallback(getListRoles));
router.post("/add_role", makeCallback(postAddRole));
router.put("/edit_role/:Code", makeCallback(putEditRole));

module.exports = router;
