
  const {
    c_selectAllItems,
    c_selectOneItem
  } = require("../../controllers/pa_items/index");
  
  const routes = ({ router, makeExpressCallback  }) => {
   
    router.post("/select", makeExpressCallback(c_selectAllItems));

    router.post("/select/:id", makeExpressCallback(c_selectOneItem));
   
    return router;
  };
 
  module.exports = routes;
  