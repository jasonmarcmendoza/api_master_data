const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../express-callback/index");

const routes = require("./routes");


const endPoints = routes({ router, makeExpressCallback });


const services = Object.freeze({
    endPoints
});

module.exports = services;

module.exports = {
    endPoints
};


module.exports = router;
