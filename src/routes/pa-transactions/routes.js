const {
    c_addRequest,
    c_selectAllTransactions,
    c_selectOneTransaction,
    c_selectAllRequests,
    // c_cancelTransaction,
    c_disapproveRequest,
    c_approveRequest,
    c_selectAllLevels,
    c_selectApprovalDetails
  } = require("../../controllers/pa_transactions/index");
  
  const routes = ({ router, makeExpressCallback  }) => {
   
    router.post("/add", makeExpressCallback(c_addRequest))

    router.post("/select", makeExpressCallback(c_selectAllTransactions))

    router.post("/select/:id", makeExpressCallback(c_selectOneTransaction))

    // router.put("/cancel/:id", makeExpressCallback(c_cancelTransaction))


    router.post("/requests/select", makeExpressCallback(c_selectAllRequests))

    router.put("/requests/disapprove/:id", makeExpressCallback(c_disapproveRequest))

    router.put("/requests/approve/:id", makeExpressCallback(c_approveRequest))

    router.post("/levels/select", makeExpressCallback(c_selectAllLevels))

    router.post("/approvals/select/:id", makeExpressCallback(c_selectApprovalDetails))
  
    return router;
  };
 
  module.exports = routes;
  