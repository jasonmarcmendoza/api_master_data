const express = require("express");
const router = express.Router();
const {
  getListPaymentTerms
} = require("../../../controllers/sap/payment_terms");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListPaymentTerms));

module.exports = router;
