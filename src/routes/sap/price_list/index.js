const express = require("express");
const router = express.Router();
const { getPriceList } = require("../../../controllers/sap/price_list");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getPriceList));

module.exports = router;
