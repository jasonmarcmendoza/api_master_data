const express = require("express");
const router = express.Router();
const { getListCountries } = require("../../../controllers/sap/countries");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListCountries));

module.exports = router;
