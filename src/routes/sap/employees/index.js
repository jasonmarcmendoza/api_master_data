const express = require("express");
const router = express.Router();

const { getEmployeeInfoByID } = require("../../../controllers/sap/employees/");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getEmployeeInfoByID));

module.exports = router;
