const express = require("express");
const router = express.Router();
const { getListBPGroups } = require("../../../controllers/sap/bp_groups");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListBPGroups));

module.exports = router;
