const express = require("express");
const router = express.Router();
const {
  getListChartOfAccounts
} = require("../../../controllers/sap/chart_of_accounts");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListChartOfAccounts));

module.exports = router;
