const express = require("express");
const router = express.Router();
const {
  getListSalesEmployees
} = require("../../../controllers/sap/sales_employees");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListSalesEmployees));

module.exports = router;
