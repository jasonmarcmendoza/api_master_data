const express = require("express");
const router = express.Router();
const { getListStates } = require("../../../controllers/sap/states");

const makeCallback = require("../../../express-callback");

router.get("/", makeCallback(getListStates));

module.exports = router;
