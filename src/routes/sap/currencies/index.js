const express = require("express");
const router = express.Router();
const { getListCurrencies } = require("../../../controllers/sap/currencies");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListCurrencies));

module.exports = router;
