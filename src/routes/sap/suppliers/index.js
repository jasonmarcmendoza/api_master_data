const express = require("express");
const router = express.Router();

const { getListSuppliers } = require("../../../controllers/sap/suppliers");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListSuppliers));

module.exports = router;
