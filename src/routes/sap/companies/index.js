const express = require("express");
const router = express.Router();

const { getListCompanies } = require("../../../controllers/sap/companies");

const makeCallback = require("../../../express-callback");

router.get("/", makeCallback(getListCompanies));

module.exports = router;
