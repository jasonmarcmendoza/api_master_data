const express = require("express");
const router = express.Router();

const { getListCustomers } = require("../../../controllers/sap/customers");

const makeCallback = require("../../../express-callback");

router.get("/:CompanyDB", makeCallback(getListCustomers));

module.exports = router;
